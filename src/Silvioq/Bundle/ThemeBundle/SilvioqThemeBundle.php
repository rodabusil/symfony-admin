<?php

namespace Silvioq\Bundle\ThemeBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\Compiler\AddBlockPass;

class SilvioqThemeBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new AddBlockPass());
    }
}
