<?php
namespace  Silvioq\Bundle\ThemeBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ThemeInstallCommand extends  Command
{
    /**
     * @var string
     */
    private $theme;
    
    /**
     * @var string
     */
    private $outputDir;
    
    /**
     * @var string
     */
    private $rootPath;
    
    /**
     * @var string
     */
    private $resourcesDir;
    
    public function __construct( $theme, $outputDir, $rootPath, $resourcesDir = null )
    {
        $this->theme = $theme;
        $this->outputDir = $outputDir . DIRECTORY_SEPARATOR . $theme;
        $this->rootPath = $rootPath;
        $this->resourcesDir = $resourcesDir ? $resourcesDir : ( __DIR__ . '/../Resources' );
        
        parent::__construct();
    }
    
    protected  function  configure()
    {
        $this->setName( 'silvioq:theme:install' )
            ->setDescription( 'Install theme on public web dir' )
            ->addArgument( 'theme', InputArgument::OPTIONAL, 'Theme name', $this->theme )
            ->addOption( 'output-dir', 'd', InputOption::VALUE_REQUIRED,
                    'Output directory', $this->outputDir )
            ->addOption( 'overwrite', null, InputOption::VALUE_NONE, 'Overwrite current theme' )
            ;
    }
    
    protected function execute( InputInterface $input, OutputInterface $output)
    {
        $theme = $input->getArgument('theme');
        $outputDir = $input->getOption('output-dir');
        $overwrite = $input->getOption('overwrite');

        $io = new SymfonyStyle( $input, $output );
        $io->title( 'Theme install tool' );

        $outputDir = $this->rootPath . DIRECTORY_SEPARATOR . $outputDir;

        // check if theme exists
        $themeDir = $this->resourcesDir . '/themes/' . $theme;
        if( !is_dir( $themeDir ) && !is_readable( $themeDir ) )
        {
            $io->error( sprintf( '%s is not readable dir theme. Is %s theme installed?',
                    $themeDir, $theme ) );
            return 255;
        }

        // check target
        if( file_exists( $outputDir ) && !$overwrite )
        {
            $io->error( sprintf( '%s dir exists. Remove first o re-run this command with overwrite option',
                    $outputDir) );
            return 255;
        }
        
        if( !file_exists( $outputDir ) ){
            if( !@mkdir($outputDir, 0755, true) )
            {
                $io->error( sprintf( 'Can\'t create %s dir', $outputDir ) );
                return 255;
            }
        }

        $count = $this->recursive_copy( $themeDir,  $outputDir );
        $io->success( sprintf( "Files copied: %d", $count ) );
        return 0;
    }
    
    /**
     * Inspired on @see http://php.net/manual/es/function.copy.php#91010
     */
    private function recursive_copy($src,$dst) {
        $dir = opendir($src);
        $count = 0;
        @mkdir($dst, 0755);
 
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if ( is_dir($src . '/' . $file) ) {
                    $count += $this->recursive_copy($src . '/' . $file,$dst . '/' . $file);
                }
                else {
                    copy($src . '/' . $file,$dst . '/' . $file);
                    $count ++;
                }
            }
        }
        closedir($dir);
        return $count;
    }

}
