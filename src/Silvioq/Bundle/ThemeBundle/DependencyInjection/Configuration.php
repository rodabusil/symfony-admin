<?php

namespace Silvioq\Bundle\ThemeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('silvioq_theme');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode->children()
            ->booleanNode( 'help_extension' )
                ->defaultTrue()
            ->end( )
            ->scalarNode('web_path')
                ->defaultValue('themes')
            ->end( )
            ->arrayNode('theme')
                ->prototype('scalar')->end()
                ->defaultValue(['adminLTE'])
                ->beforeNormalization()
                    ->ifString()
                    ->then(function($v){ return [$v]; })
                ->end( )
            ->end( )
            ->arrayNode('notifications')
              ->addDefaultsIfNotSet()
              ->children()
                ->booleanNode( 'enabled' )
                    ->defaultTrue()
                ->end( )
                ->booleanNode( 'show_icon_on_zero' )
                    ->defaultTrue()
                ->end( )
                ->scalarNode( 'view_all_path' )
                    ->defaultNull()
                ->end( )
                ->enumNode( 'badge_type' )
                    ->values(['warning','success', 'danger','info'])
                    ->defaultNull()
                ->end( )
              ->end( )
            ->end( )
            ->scalarNode('twig_form_resource')
                ->defaultValue( 'SilvioqThemeBundle:layout:form-theme.html.twig' )
            ->end( )
        ->end( );

        return $treeBuilder;
    }
}
