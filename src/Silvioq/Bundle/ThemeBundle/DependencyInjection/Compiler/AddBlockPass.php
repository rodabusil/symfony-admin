<?php

namespace Silvioq\Bundle\ThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Silvioq\Component\Theme\Block\Provider\MenuProviderInterface;
use Silvioq\Component\Theme\Block\Provider\UserProviderInterface;
use Silvioq\Component\Theme\Block\Provider\NotificationProviderInterface;
use Symfony\Component\DependencyInjection\Reference;

class AddBlockPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if( !$container->has('silvioq.theme.block.builder') )
        {
            return;
        }

        $definition = $container->findDefinition('silvioq.theme.block.builder');
        $notificationEnabled = $container->hasParameter('silvioq_theme.notificationsEnabled') && $container->getParameter('silvioq_theme.notificationsEnabled');

        $taggedServices = $container->findTaggedServiceIds('silvioq.theme.block');
        foreach( $taggedServices as $id => $tags )
        {
            $service = $container->getDefinition($id);
            $class = new \ReflectionClass($service->getClass());
            foreach( $tags as $tag )
            {
                if( !isset( $tag['type'] ) )
                    throw new \LogicException( sprintf( 'Service %s must declare \'type\' on silvioq.theme.block tag', $id ) );

                switch($tag['type'])
                {
                    case 'menu.top':
                        if( $class->implementsInterface( MenuProviderInterface::class ) )
                            $definition->addMethodCall('addTopMenuProvider', [ new Reference($id) ] );
                        else
                            throw new \LogicException( sprintf( 'Service %s must implements %s', $id, MenuProviderInterface::class ) );
                        break;

                    case 'menu.side':
                        if( $class->implementsInterface( MenuProviderInterface::class ) )
                            $definition->addMethodCall('addSideMenuProvider', [ new Reference($id) ] );
                        else
                            throw new \LogicException( sprintf( 'Service %s must implements %s', $id, MenuProviderInterface::class ) );
                        break;

                    case 'user':
                        if( $class->implementsInterface( UserProviderInterface::class ) )
                            $definition->addMethodCall('addUserProvider', [ new Reference($id) ] );
                        else
                            throw new \LogicException( sprintf( 'Service %s must implements %s', $id, UserProviderInterface::class ) );
                        break;

                    case 'notification':
                        if (!$notificationEnabled)
                            break;

                        if( $class->implementsInterface( NotificationProviderInterface::class ) )
                            $definition->addMethodCall('addNotificationProvider', [ new Reference($id) ] );
                        else
                            throw new \LogicException( sprintf( 'Service %s must implements %s', $id, NotificationProviderInterface::class ) );
                        break;

                    default:
                        throw new \LogicException( sprintf( 'Type %s is invalid, must be menu.top, menu.side, user or notification', $tag['type'] ) );
                }
            }
        }
    }
}
// vim:sw=4 ts=4 sts=4 et

