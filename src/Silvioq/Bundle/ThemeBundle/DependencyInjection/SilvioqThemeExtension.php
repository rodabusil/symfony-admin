<?php

namespace Silvioq\Bundle\ThemeBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SilvioqThemeExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if( $container->hasParameter('kernel.bundles') )
            $bundles = $container->getParameter('kernel.bundles');
        else
            $bundles = [];

        if( isset( $bundles['AsseticBundle'] ) )
        {
            $aAsseticBundle = $container->getParameter('assetic.bundles');
            array_push( $aAsseticBundle, 'SilvioqThemeBundle' );
            $container->setParameter('assetic.bundles', $aAsseticBundle);
        }

        if( isset( $bundles['TwigBundle'] ) )
        {
            $aForms = $container->getParameter('twig.form.resources');
            array_push( $aForms, $config['twig_form_resource'] );
            $container->setParameter( 'twig.form.resources', $aForms );
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $definition = $container->getDefinition('silvioq.theme.block.params');
        
        if( isset( $config['notifications'] ) && $config['notifications']['enabled'] )
        {
            $definition->addMethodCall("set", [ "showNotificationsOnZero", $config['notifications']['show_icon_on_zero'] ] );
            $definition->addMethodCall("set", [ "showNotificationsOnZero", $config['notifications']['show_icon_on_zero'] ] );
            $definition->addMethodCall("set", [ "viewAllNotificationsUrl", $config['notifications']['view_all_path'] ] );
            $definition->addMethodCall("set", [ "badgeType", $config['notifications']['badge_type'] ] );
            $definition->addMethodCall("set", [ "notificationsEnabled",  true ] );
            $container->setParameter('silvioq_theme.notificationsEnabled', true);
        }

        // Configure install theme command
        $definition = $container->getDefinition('silvioq.theme.install.command');
        $arguments = $definition->getArguments();
        $arguments[0] = $config['theme'][0];
        $arguments[1] = $config['web_path'];
        $definition->setArguments($arguments);

        // Configure Asset extension
        $definition = $container->getDefinition('silvioq.theme.asset.extension');
        $arguments = $definition->getArguments();
        $arguments[1] = $config['web_path'] . '/' . $config['theme'][0] ;
        $definition->setArguments($arguments);

        // Configure Alternative theme loader
        $definition = $container->getDefinition('silvioq.theme.twig.loader');
        $arguments = $definition->getArguments();
        $arguments[1] = $config['theme'];
        $definition->setArguments($arguments);

        // Load Form help extension
        if( $config['help_extension'] )
            $loader->load( 'service_form_help.yml' );
    }

}
