<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Loader;

use Twig\Loader\FilesystemLoader;

class AlternativeThemeLoader extends FilesystemLoader
{

    /**
     * @param FilesystemLoader $loader  Original Twig file system loader
     * @param string|array $themeName
     * TODO: Move all configurations to SilvioqThemeExtension class
     */
    public function __construct( FilesystemLoader $loader, $themeName )
    {
        if( is_string($themeName) ) $themeName = [ $themeName ];
        foreach( $loader->getNamespaces() as $ns )
        {
            foreach( $loader->getPaths($ns) as $path )
            {
                foreach( $themeName as $t )
                {
                    $path2 = $path . "/" . $t;
                    if( is_dir( $path2 ) )
                        $this->addPath( $path2, $ns === '__main__' ? 'AltApp': $ns );
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getSourceContext($name)
    {
        $alternativeName = $this->convertName($name);
        if( false === $alternativeName )
            throw new \Twig_Error_Loader( sprintf( '%s is not found', $name ) );

        return parent::getSourceContext($alternativeName);
    }

    /**
     * {@inheritdoc}
     */
    public function getSource($name)
    {
        $alternativeName = $this->convertName($name);
        if( false === $alternativeName )
            throw new \Twig_Error_Loader( sprintf( '%s is not found', $name ) );

        return parent::getSource($alternativeName);
    }

    /**
     * {@inheritdoc}
     */
    public function exists($name)
    {
        $alternativeName = $this->convertName($name);
        if( false === $alternativeName )
            return false;

        return parent::exists( $alternativeName );
    }

    /**
     * {@inheritdoc}
     */
    public function isFresh($name, $time)
    {
        $alternativeName = $this->convertName($name);
        if( false === $alternativeName )
            throw new \Twig_Error_Loader( sprintf( '%s is not found', $name ) );

        return parent::isFresh( $alternativeName, $time );
    }

    /**
     * {@inheritdoc}
     */
    public function getCacheKey($name)
    {
        $alternativeName = $this->convertName($name);
        if( false === $alternativeName )
            throw new \Twig_Error_Loader( sprintf( '%s is not found', $name ) );

        return parent::getCacheKey( $alternativeName );
    }

    /**
     * Convert from symfony name type (like MyBundle:default:index.html.twig
     * to original twig @My/${themeName}/default/index.html.twig
     */
    private function convertName($name)
    {
        if( isset( $name[0] ) && '@' === $name[0] )
            return $name;

        if( preg_match( '/^([^:\/]+)Bundle:(.*)$/', $name, $matches ) )
        {
            $bundle = '@' . $matches[1];
            $filePath = preg_replace( '/:/', '/', $matches[2] );
            return $bundle . '/' . $filePath;
        }
        else if( preg_match( '/^::(.*)$/', $name, $matches ) )
        {
            return '@AltApp/' . $matches[1];
        }
        return false;
    }

}

