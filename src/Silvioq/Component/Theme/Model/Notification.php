<?php

namespace Silvioq\Component\Theme\Model;


class Notification implements NotificationInterface
{


    protected $type;

    protected $message;

    protected $icon;

    protected $url;

    /**
     * @var string
     */
    protected $detail;

    function __construct($message = null, $url = '', $type = 'info', $icon = 'fa fa-warning', $detail = null)
    {
        $this->message = $message;
        $this->type    = $type;
        $this->icon    = $icon;
        $this->url     = $url;
        $this->detail = $detail;
    }


    /**
     * @param mixed $message
     *
     * @return $this
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $icon
     *
     * @return $this
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string $detail
     */
    public function setDetail($detail)
    {
        $this->detail = $detail;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDetail()
    {
        return $this->detail;
    }

}
