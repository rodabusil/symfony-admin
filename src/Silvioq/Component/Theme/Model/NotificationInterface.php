<?php

namespace Silvioq\Component\Theme\Model;


interface NotificationInterface {
    public function getMessage();
    public function getType();
    public function getIcon();
    public function getUrl();
    public function getDetail();
}
