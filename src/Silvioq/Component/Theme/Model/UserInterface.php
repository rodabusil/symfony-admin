<?php

namespace Silvioq\Component\Theme\Model;

interface UserInterface {
    public function getAvatar();
    public function getUsername();
    public function getName();
    public function getMemberSince();
    public function isOnline();
    public function getTitle();
    public function getProfileRoute();
    public function getProfileRouteArgs();
    public function getLogoutRoute();
}
