<?php

namespace Silvioq\Component\Theme\Param;

use Symfony\Component\HttpFoundation\ParameterBag;

class Parameter extends ParameterBag
{
    public function getShowNotificationsOnZero()
    {
        return $this->get('showNotificationsOnZero', false);
    }

    public function getViewAllNotificationsUrl()
    {
        return $this->get('viewAllNotificationsUrl', null);
    }

    public function getNotificationsEnabled()
    {
        return $this->get('notificationsEnabled', false);
    }

    public function getBadgeType()
    {
        return $this->get('badgeType', null);
    }
}
