<?php

namespace Silvioq\Component\Theme\Twig;

use Silvioq\Component\AdminTool\Route\RouteCheckerInterface;

/**
 * @author silvioq
 *
 * Agrega la función path_exists a twig
 */
class PathExistsExtension extends \Twig_Extension
{
    /** @var RouteCheckerInterface */
    private $routeChecker;

    public function __construct( RouteCheckerInterface $routeChecker )
    {
        $this->routeChecker = $routeChecker;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('path_exists', [ $this, 'pathExists' ] ),
        ];
    }

    /**
     * @return bool
     */
    public function pathExists( $path )
    {
        return $this->routeChecker->routeExists( $path );
    }

    /**
     * Twig < 1.26 compatibility
     */
    public function getName()
    {
        return 'silvioq.theme.path_exists.extension';
    }
}
// vim:sw=4 ts=4 sts=4 et
