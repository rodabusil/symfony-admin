<?php

namespace Silvioq\Component\Theme\Twig;

use Silvioq\Component\AdminTool\Asset\AssetTheme;
use Symfony\Component\Asset\Packages;

/**
 * @author silvioq
 *
 * Adds asset_theme function
 */
class AssetThemeExtension extends \Twig_Extension
{
    /**
     * @var Packages
     */
    private $packages;
    
    /**
     * @var string
     */
    private $pathTheme;
    
    /**
     * @var AssetTheme
     */
    private $assetTheme = null;

    public function __construct( Packages $packages, $pathTheme )
    {
        $this->packages = $packages;
        $this->pathTheme = $pathTheme;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('asset_theme', [ $this, 'getUrl' ] ),
        ];
    }

    public function getUrl($name)
    {
        if( null === $this->assetTheme )
            $this->assetTheme = new AssetTheme($this->packages, $this->pathTheme );

        return $this->assetTheme->getUrl($name);
    }

    /**
     * Twig < 1.26 compatibility
     */
    public function getName()
    {
        return 'silvioq.theme.asset.extension';
    }

}
// vim:sw=4 ts=4 sts=4 et
