<?php

namespace Silvioq\Component\Theme\Twig;

use Silvioq\Component\Theme\Block\BlockBuilderInterface;
use Silvioq\Component\Theme\Block\BlockDeferredManager;
use Symfony\Component\HttpFoundation\ParameterBag;
use Silvioq\Component\Theme\Param\Parameter;

/**
 * @author silvioq
 *
 * Agrega el objeto global silvioq_macro
 */
class BlockExtension extends \Twig_Extension  implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var BlockBuilderInterface
     */
    private $builder;
    
    /**
     * @var ParameterBag
     */
    private $param;

    public function __construct( BlockBuilderInterface $builder, ParameterBag $param=null )
    {
        $this->builder = $builder;
        $this->param = $param;
    }

    public function getGlobals()
    {
        return [
            'silvioq_block' => new BlockDeferredManager($this->builder),
            'silvioq_param' => new Parameter($this->param->all() ),
        ];
    }

    /**
     * Twig < 1.26 compatibility
     */
    public function getName()
    {
        return 'silvioq.theme.block.extension';
    }

}
// vim:sw=4 ts=4 sts=4 et
