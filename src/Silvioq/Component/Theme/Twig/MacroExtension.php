<?php

namespace Silvioq\Component\Theme\Twig;

use Silvioq\Component\AdminTool\Asset\MacroAsset;
use Symfony\Component\Asset\Packages;

/**
 * @author silvioq
 *
 * Agrega el objeto global silvioq_macro
 */
class MacroExtension extends \Twig_Extension  implements \Twig_Extension_GlobalsInterface
{
    /**
     * @var Packages
     */
    private $packages;

    /**
     * @var string
     */
    private $prefix;

    public function __construct( Packages $packages, $prefix = null )
    {
        $this->packages = $packages;
        $this->prefix = $prefix;
    }

    public function getGlobals()
    {
        return [ 'silvioq_macro' => new MacroAsset($this->packages, $this->prefix) ];
    }

    /**
     * Twig < 1.26 compatibility
     */
    public function getName()
    {
        return 'silvioq.theme.macro.extension';
    }

}
// vim:sw=4 ts=4 sts=4 et
