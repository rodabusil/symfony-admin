<?php

namespace Silvioq\Component\Theme\Block;

use Silvioq\Component\Theme\Model\MenuItemInterface;
use Silvioq\Component\Theme\Model\UserInterface;
use Silvioq\Component\Theme\Model\NotificationInterface;

class BlockDeferredManager implements BlockManagerInterface
{
    /**
     * @var BlockManager
     */
    private $manager = false;

    /**
     * @var BlockBuilderInterface
     */
    private $builder;

    public function __construct(BlockBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getTopMenus()
    {
        $this->init();
        return $this->manager->getTopMenus();
    }

    /**
     * {@inheritdoc}
     */
    public function hasTopMenus()
    {
        $this->init();
        return $this->manager->hasTopMenus();
    }

    /**
     * {@inheritdoc}
     */
    public function getSideMenus()
    {
        $this->init();
        return $this->manager->getSideMenus();
    }

    /**
     * {@inheritdoc}
     */
    public function hasSideMenus()
    {
        $this->init();
        return $this->manager->hasSideMenus();
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        $this->init();
        return $this->manager->getUser();
    }

    /**
     * {@inheritdoc}
     */
    public function getNotifications()
    {
        $this->init();
        return $this->manager->getNotifications();
    }

    /**
     * {@inheritdoc}
     */
    public function hasNotifications()
    {
        $this->init();
        return $this->manager->hasNotifications();
    }

    /**
     *
     */
    private function init()
    {
        if( false === $this->manager )
            $this->manager = $this->builder->build();
    }
}
// vim:sw=4 ts=4 sts=4 et
