<?php

namespace Silvioq\Component\Theme\Block;

use Silvioq\Component\Model\MenuItemInterface;
use Silvioq\Component\Model\UserInterface;
use Silvioq\Component\Model\NotificationInterface;

interface BlockManagerInterface
{
    /**
     * @return MenuItemInterface[]
     */
    public function getTopMenus();

    /**
     * @return bool
     */
    public function hasTopMenus();

    /**
     * @return MenuItemInterface[]
     */
    public function getSideMenus();

    /**
     * @return bool
     */
    public function hasSideMenus();

    /**
     * @return UserInterface|null
     */
    public function getUser();

    /**
     * @return NotificationInterface[]
     */
    public function getNotifications();

    /**
     * @return bool
     */
    public function hasNotifications();
}
// vim:sw=4 ts=4 sts=4 et
