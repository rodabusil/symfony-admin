<?php

namespace Silvioq\Component\Theme\Block\Provider;


use Silvioq\Component\Theme\Model\NotificationInterface;

interface NotificationProviderInterface
{
    /**
     * @return NotificationInterface[]
     */
    public function getNotifications();
}
// vim:sw=4 ts=4 sts=4 et
