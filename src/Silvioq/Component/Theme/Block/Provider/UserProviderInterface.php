<?php

namespace Silvioq\Component\Theme\Block\Provider;

use Silvioq\Component\Theme\Model\UserInterface;

interface UserProviderInterface
{
    /**
     * @return UserInterface|null
     */
    public function getUser();
}
// vim:sw=4 ts=4 sts=4 et
