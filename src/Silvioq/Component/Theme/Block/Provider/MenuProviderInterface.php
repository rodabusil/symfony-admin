<?php

namespace Silvioq\Component\Theme\Block\Provider;

use Silvioq\Component\Theme\Model\MenuItemInterface;

interface MenuProviderInterface
{
    /**
     * @return MenuItemInterface[]
     */
    public function getItems();
}

// vim:sw=4 ts=4 sts=4 et
