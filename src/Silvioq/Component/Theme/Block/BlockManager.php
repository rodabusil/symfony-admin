<?php

namespace Silvioq\Component\Theme\Block;

use Silvioq\Component\Theme\Model\MenuItemInterface;
use Silvioq\Component\Theme\Model\UserInterface;
use Silvioq\Component\Theme\Model\NotificationInterface;

class BlockManager implements BlockManagerInterface
{

    /**
     * @var MenuItemInterface[]
     */
    private $topMenus;

    /**
     * @var MenuItemInterface[]
     */
    private $sideMenus;

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var Notification[]
     */
    private $notifications;


    public function __construct( array $topMenus, array $sideMenus, UserInterface $user = null, array $notifications )
    {
        foreach( $topMenus as $menu )
        {
            if( !($menu instanceof MenuItemInterface) )
                throw new \InvalidArgumentException( 'Argument topMenus must be MenuItemInterface[]' );
        }
        foreach( $sideMenus as $menu )
        {
            if( !($menu instanceof MenuItemInterface) )
                throw new \InvalidArgumentException( 'Argument sideMenus must be MenuItemInterface[]' );
        }
        foreach( $notifications as $not )
        {
            if( !($not instanceof NotificationInterface) )
                throw new \InvalidArgumentException( 'Argument notifications must be NotificationInterface[]' );
        }

        $this->topMenus = $topMenus;
        $this->sideMenus = $sideMenus;
        $this->user = $user;
        $this->notifications = $notifications;
    }

    /**
     * {@inheritdoc}
     */
    public function getTopMenus()
    {
        return $this->topMenus;
    }

    /**
     * {@inheritdoc}
     */
    public function hasTopMenus()
    {
        return count($this->topMenus) > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getSideMenus()
    {
        return $this->sideMenus;
    }

    /**
     * {@inheritdoc}
     */
    public function hasSideMenus()
    {
        return count($this->sideMenus) > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * {@inheritdoc}
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * {@inheritdoc}
     */
    public function hasNotifications()
    {
        return count($this->notifications) > 0;
    }
}
// vim:sw=4 ts=4 sts=4 et
