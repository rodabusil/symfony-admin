<?php

namespace Silvioq\Component\Theme\Block;

use Silvioq\Component\Theme\Block\Provider\MenuProviderInterface;
use Silvioq\Component\Theme\Block\Provider\UserProviderInterface;
use Silvioq\Component\Theme\Block\Provider\NotificationProviderInterface;

class BlockBuilder implements BlockBuilderInterface
{
    /**
     * @var MenuProviderInterface[]
     */
    private $topMenuProviders = [];

    /**
     * @var MenuProviderInterface[]
     */
    private $sideMenuProviders = [];

    /**
     * @var UserProviderInterface[]
     */
    private $userProviders = [];

    /**
     * @var Notification[]
     */
    private $notificationProviders = [];

    /**
     * @param MenuProviderInterface $menuTopProvider
     * @return self
     */
    public function addTopMenuProvider( MenuProviderInterface $menuProvider )
    {
        $this->topMenuProviders[] = $menuProvider;
        return $this;
    }

    /**
     * @param MenuProviderInterface $menuTopProvider
     * @return self
     */
    public function addSideMenuProvider( MenuProviderInterface $menuProvider )
    {
        $this->sideMenuProviders[] = $menuProvider;
        return $this;
    }

    /**
     * @param UserProviderInterface $userProvider
     * @return self
     */
    public function addUserProvider( UserProviderInterface $userProvider )
    {
        $this->userProviders[] = $userProvider;
        return $this;
    }

    /**
     * @param NotificationProviderInterface $notificationProvider
     * @return self
     */
    public function addNotificationProvider( NotificationProviderInterface $notificationProvider )
    {
        $this->notificationProviders[] = $notificationProvider;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function build()
    {
        $topMenus = [];
        $sideMenus = [];
        $user = null;
        $notifications = [];
        
        foreach( $this->topMenuProviders as $menuProvider )
        {
            foreach( $menuProvider->getItems() as $menu ) $topMenus[] = $menu;
        }

        foreach( $this->sideMenuProviders as $menuProvider )
        {
            foreach( $menuProvider->getItems() as $menu ) $sideMenus[] = $menu;
        }

        foreach( $this->userProviders as $userProvider )
        {
            if( $user = $userProvider->getUser() ) break;
        }

        foreach( $this->notificationProviders as $notificationProvider )
        {
            foreach( $notificationProvider->getNotifications() as $notification ) $notifications[] = $notification;
        }

        return new BlockManager( $topMenus, $sideMenus, $user, $notifications );
    }
}
// vim:sw=4 ts=4 sts=4 et
