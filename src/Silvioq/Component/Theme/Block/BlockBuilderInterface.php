<?php

namespace Silvioq\Component\Theme\Block;

interface BlockBuilderInterface
{
    /**
     * @return BlockManagerInterface
     */
    public function build();
    
}
// vim:sw=4 ts=4 sts=4 et
