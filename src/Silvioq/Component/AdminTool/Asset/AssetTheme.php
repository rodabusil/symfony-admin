<?php

namespace Silvioq\Component\AdminTool\Asset;

use Symfony\Component\Asset\Packages;
/**
 * Clase para hacer globales las macros
 * tal como trabaja el twig 1.3
 */
class AssetTheme
{
    /**
     * @var Packages
     */
    private $packages;
    
    /**
     * @var string
     */
    private $pathPrefix;

    public function __construct( Packages $packages, $pathPrefix )
    {
        $this->packages = $packages;
        $this->pathPrefix = $pathPrefix;
    }
    
    public function getUrl($name)
    {
        return $this->packages->getUrl( $this->pathPrefix . "/" . $name );
    }
}

