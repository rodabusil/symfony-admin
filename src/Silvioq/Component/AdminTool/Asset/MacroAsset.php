<?php

namespace Silvioq\Component\AdminTool\Asset;

use Symfony\Component\Asset\Packages;
/**
 * Clase para hacer globales las macros
 * tal como trabaja el twig 1.3
 */
class MacroAsset
{
    /**
     * @var Packages
     */
    private $packages;
    
    /**
     * @var string
     */
    private $pathPrefix;

    const PD = "bundles/silvioqthemelte/plugins";

    public function __construct( Packages $packages, $pathPrefix = null )
    {
        $this->packages = $packages;
        if( null === $pathPrefix )
            $this->pathPrefix = self::PD;
        else
            $this->pathPrefix = $pathPrefix;
    }

    public function plugin_css($plugin_file)
    {
        @trigger_error( "silvioq_macro.plugin_css(file) is deprecated. Use <link rel=\"stylesheet\" href=\"{{ asset_theme(file) }}\" /> instead",
                E_USER_DEPRECATED );
        $url = $this->packages->getUrl( $this->pathPrefix . "/" . $plugin_file );
        $ret = '<link rel="stylesheet" href="' . $url . '" />';
        return new \Twig_Markup( $ret, 'utf-8' );
    }

    public function plugin_js($plugin_file)
    {
        @trigger_error( "silvioq_macro.plugin_js(file) is deprecated. Use <script src=\"{{ asset_theme(file) }}\" type=\"text/javascript\"></script> instead",
                E_USER_DEPRECATED );
        $url = $this->packages->getUrl( $this->pathPrefix . "/" . $plugin_file );
        $ret = '<script src="' . $url . '"></script>';
        return new \Twig_Markup( $ret, 'utf-8' );
    }
}
