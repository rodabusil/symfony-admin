<?php

namespace Silvioq\Component\AdminTool\Route;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

class RouteChecker implements RouteCheckerInterface
{
    /** @var RouterInterface */
    private $router;

    public function __construct( RouterInterface $router )
    {
        $this->router = $router;
    }

    /**
     * @param $route string
     * @return bool
     */
    public function routeExists( $route )
    {
        try
        {
            $this->router->generate( $route );
        }
        catch( RouteNotFoundException $e )
        {
            return false;
        }
        catch( MissingMandatoryParametersException $m )
        {
            return true;
        }
        return true;
    }
}
// vim:sw=4 ts=4 sts=4 et
