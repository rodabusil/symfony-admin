<?php

namespace Silvioq\Component\AdminTool\Route;

interface RouteCheckerInterface
{
    /**
     * @param $name string   Route name
     * @return bool
     */
    public function routeExists( $name );
}
