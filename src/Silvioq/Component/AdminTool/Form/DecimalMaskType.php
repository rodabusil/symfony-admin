<?php

namespace  Silvioq\Component\AdminTool\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class DecimalMaskType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->addViewTransformer(new DataTransformer\FlexibleDecimalToLocaleTransformer(
                $options['scale'],
                $options['grouping_symbol'],
                $options['decimal_symbol'],
                null,
                $options['divisor']
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $fmt = new \NumberFormatter( \Locale::getDefault(), \NumberFormatter::DECIMAL );

        $resolver->setDefaults(array(
            'scale' => 2,
            'grouping_symbol' => $fmt->getSymbol(\NumberFormatter::GROUPING_SEPARATOR_SYMBOL),
            'decimal_symbol' => $fmt->getSymbol(\NumberFormatter::DECIMAL_SEPARATOR_SYMBOL),
            'divisor' => 1,
            'compound' => false,
        ));

        $resolver->setAllowedTypes('scale', 'int');
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView( $view, $form, $options );

        $mask = [
            'grouping_symbol' => $options['grouping_symbol'],
            'decimal_symbol' => $options['decimal_symbol'],
            'scale' => $options['scale'],
        ];

        $view->vars['mask'] = $mask;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mask_js';
    }
}
// vim:sw=4 ts=4 sts=4 et
