<?php

namespace  Silvioq\Component\AdminTool\Form;

use Symfony\Component\Form\Extension\Core\Type\DateType as SymfonyDateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class  DateType  extends  SymfonyDateType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions( $resolver );
        $resolver->setDefault( 'widget', 'single_text' );
    }
}


