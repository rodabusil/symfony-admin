<?php
namespace  Silvioq\Component\AdminTool\Form;

use Symfony\Component\Form\Extension\Core\Type\TimeType as SymfonyTimeType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class  TimeType  extends  SymfonyTimeType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions( $resolver );
        $resolver->setDefault( 'widget', 'single_text' );
    }
}
