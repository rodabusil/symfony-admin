<?php

namespace Silvioq\Component\AdminTool\Form\DataTransformer;

use Silvioq\Component\AdminTool\Form\MoneyType;
use Symfony\Component\Form\Extension\Core\DataTransformer\MoneyToLocalizedStringTransformer;

/**
 * Transforms between a number type and a localized number with grouping
 * (each thousand) and comma separators.
 *
 */
class FlexibleDecimalToLocaleTransformer extends MoneyToLocalizedStringTransformer
{
    private $groupingSymbol;
    private $decimalSymbol;

    /**
     * @param mixed groupingSymbol  null or symbol
     * @param mixed decimalSymbol   null or symbol
     */
    public function __construct($scale = 2, $groupingSymbol = null, $decimalSymbol = false, $roundingMode = self::ROUND_HALF_UP, $divisor = 1)
    {
        parent::__construct($scale, null !== $groupingSymbol, $roundingMode, $divisor);

        $this->groupingSymbol = $groupingSymbol;
        $this->decimalSymbol = $decimalSymbol;
    }

    /**
     * Returns a preconfigured \NumberFormatter instance.
     *
     * @return \NumberFormatter
     */
    protected function getNumberFormatter()
    {
        $formatter = parent::getNumberFormatter();

        if (null !== $this->groupingSymbol) {
            $formatter->setSymbol(\NumberFormatter::GROUPING_SEPARATOR_SYMBOL, $this->groupingSymbol);
        }
        if (null !== $this->decimalSymbol) {
            $formatter->setSymbol(\NumberFormatter::DECIMAL_SEPARATOR_SYMBOL, $this->decimalSymbol);
        }

        return $formatter;
    }
}
// vim:sw=4 ts=4 sts=4 et
