<?php

namespace  Silvioq\Component\AdminTool\Form;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IntlDateFormatter;

class  MonthType  extends  ChoiceType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions( $resolver );
        $resolver->setDefaults([
            'choices' => $this->getMonths(),
            'required' => false,
            'choice_translation_domain' => false,
        ]);
    }

    private function getMonths()
    {
        $months = [];
        $fmt = new IntlDateFormatter( null, IntlDateFormatter::FULL, IntlDateFormatter::NONE );
        $fmt->setPattern( "LLLL" );
        
        $date = new \DateTime();

        for( $i = 1; $i <= 12; $i ++ )
        {
            $date->setDate( 2011, $i, 1 );
            $months[$fmt->format($date)] = $i;
        }
        return $months;
    }
}
