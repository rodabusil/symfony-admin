<?php

namespace  Silvioq\Component\AdminTool\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FormType;

use Symfony\Component\Form\AbstractTypeExtension;

class  ExtensionHelp extends  AbstractTypeExtension
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setAttribute('help', $options['help']);
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        if( $options['help'] !== null ) $view->vars['help'] = $options['help'];
    }


    /**
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined( array( 'help' ) );
        $resolver->setDefaults(array(
            'help' => null,
        ));
    }
    /**
     * @return string
     */
    public function getExtendedType()
    {
        return FormType::class;
    }
}
