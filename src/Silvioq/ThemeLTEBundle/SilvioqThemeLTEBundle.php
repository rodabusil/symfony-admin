<?php

namespace Silvioq\ThemeLTEBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SilvioqThemeLTEBundle extends Bundle
{
    public function __construct()
    {
        @trigger_error( sprintf( '%s is deprectated. Use Silvioq\Bundle\ThemeBundle instead', __CLASS__), E_USER_DEPRECATED );
    }
}
