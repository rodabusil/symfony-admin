<?php

namespace Silvioq\Component\AdminTool\Asset\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Asset\Packages;
use Silvioq\Component\AdminTool\Asset\AssetTheme;

class AssetThemeTest extends TestCase
{
    public function testAssetThemeUrl()
    {
        /** @var Packages */
        $mock = $this
                ->getMockBuilder(Packages::class)
                ->disableOriginalConstructor()
                ->getMock();

        $mock->expects($this->once())
            ->method('getUrl')
            ->with($this->equalTo('theme/myTheme/css/x.css'))
            ->will($this->returnValue('/web/theme/myTheme/css/x.css'))
            ;
        $asset = new AssetTheme($mock, 'theme/myTheme' );
  
        $this->assertEquals( '/web/theme/myTheme/css/x.css', $asset->getUrl("css/x.css") );
    }
}
