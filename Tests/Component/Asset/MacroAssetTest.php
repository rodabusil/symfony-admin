<?php

namespace Silvioq\Component\AdminTool\Asset\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Asset\Packages;
use Silvioq\Component\AdminTool\Asset\MacroAsset;

class MacroAssetTest extends TestCase
{
    public function testMacroAssetPluginCss(){
        /** @var Packages */
        $mock = $this
                ->getMockBuilder(Packages::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mock->expects($this->once())
            ->method('getUrl')
            ->with($this->equalTo('bundles/silvioqthemelte/plugins/x.css'))
            ->will($this->returnValue('url'))
            ;
        $macro = new MacroAsset($mock);
        $markup = $macro->plugin_css('x.css');
        $this->assertInstanceOf( \Twig_Markup::class, $markup );
        $this->assertEquals( '<link rel="stylesheet" href="url" />', (string)$markup );
    }

    public function testMacroAssetPluginJs(){
        /** @var Packages */
        $mock = $this
                ->getMockBuilder(Packages::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mock->expects($this->once())
            ->method('getUrl')
            ->with($this->equalTo('bundles/silvioqthemelte/plugins/x.js'))
            ->will($this->returnValue('url'))
            ;
        $macro = new MacroAsset($mock);
        $markup = $macro->plugin_js('x.js');
        $this->assertInstanceOf( \Twig_Markup::class, $markup );
        $this->assertEquals( '<script src="url"></script>', (string)$markup );
    }
    
    public function testMacroAssetCustomPath()
    {
        /** @var Packages */
        $mock = $this
                ->getMockBuilder(Packages::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mock->expects($this->once())
            ->method('getUrl')
            ->with($this->equalTo('web/x.js'))
            ->will($this->returnValue('/web/x.js'))
            ;
        $macro = new MacroAsset($mock, "web" );
        $markup = $macro->plugin_css('x.js');
        $this->assertInstanceOf( \Twig_Markup::class, $markup );
        $this->assertEquals( '<link rel="stylesheet" href="/web/x.js" />', (string)$markup );
    }

    public function testDeprecationOfPluginJs()
    {
        $deprecations = [];
        error_reporting(-1);
        set_error_handler(function ($type, $msg) use (&$deprecations) {
            if (E_USER_DEPRECATED === $type) {
                $deprecations[] = $msg;
            }
        });
 
        $this->testMacroAssetPluginJs();

        $this->assertCount(1, $deprecations);
        $this->assertTrue( 0 === strpos($deprecations[0], 'silvioq_macro.plugin_js(file) is deprecated.'));
    }

    public function testDeprecationOfPluginCss()
    {
        $deprecations = [];
        error_reporting(-1);
        set_error_handler(function ($type, $msg) use (&$deprecations) {
            if (E_USER_DEPRECATED === $type) {
                $deprecations[] = $msg;
            }
        });
 
        $this->testMacroAssetPluginCss();
 
        $this->assertCount(1, $deprecations);
        $this->assertTrue( 0 === strpos($deprecations[0], 'silvioq_macro.plugin_css(file) is deprecated.'));
    }

}
