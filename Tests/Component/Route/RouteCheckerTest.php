<?php

namespace Silvioq\Component\AdminTool\Route\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Asset\Packages;
use Silvioq\Component\AdminTool\Route\RouteChecker;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\Exception\MissingMandatoryParametersException;

class RouteCheckerTest extends TestCase
{
    public function testExistentRoute()
    {
        $mockRouter = $this
                        ->getMockBuilder(RouterInterface::class)
                        ->disableOriginalConstructor()
                        ->getMock();
                        
        $mockRouter->expects($this->once())
            ->method('generate')
            ->with('my_route')
            ->will($this->returnValue('/url'))
            ;
        
        $checker = new RouteChecker($mockRouter);
        
        $this->assertTrue($checker->routeExists('my_route'));
    }

    public function testExistentButIncompleteRoute()
    {
        $mockRouter = $this
                        ->getMockBuilder(RouterInterface::class)
                        ->disableOriginalConstructor()
                        ->getMock();
                        
        $mockRouter->expects($this->once())
            ->method('generate')
            ->with('my_route')
            ->will($this->throwException(new MissingMandatoryParametersException() ))
            ;
        
        $checker = new RouteChecker($mockRouter);
        
        $this->assertTrue($checker->routeExists('my_route'));
    }

    public function testNotExistentRoute()
    {
        $mockRouter = $this
                        ->getMockBuilder(RouterInterface::class)
                        ->disableOriginalConstructor()
                        ->getMock();
                        
        $mockRouter->expects($this->once())
            ->method('generate')
            ->with('my_route')
            ->will($this->throwException(new RouteNotFoundException() ))
            ;
        
        $checker = new RouteChecker($mockRouter);
        
        $this->assertFalse($checker->routeExists('my_route'));
    }

}
