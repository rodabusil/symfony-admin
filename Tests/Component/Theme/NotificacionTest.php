<?php

namespace Silvioq\Component\Theme\Model\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Model\Notification;

class NotificationTest extends TestCase
{
    public function testNotification()
    {
        $not = new Notification( 'message', 'url', 'type', 'icon', 'my detail' );
        $this->assertSame( 'message', $not->getMessage() );
        $this->assertSame( 'url', $not->getUrl() );
        $this->assertSame( 'type', $not->getType() );
        $this->assertSame( 'icon', $not->getIcon() );
        $this->assertSame('my detail', $not->getDetail());
    }
}

