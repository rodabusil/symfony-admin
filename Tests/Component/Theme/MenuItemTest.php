<?php

namespace Silvioq\Component\Theme\Model\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Model\MenuItem;

class MenuItemTest extends TestCase
{
    public function testMenuItem()
    {
        $menu = new MenuItem(
            'id', 'label', 'route', [ 'route' => 'arg' ], 'icon', 'badge', 'badgeColor' );
            
        $this->assertSame( 'id', $menu->getIdentifier() );
        $this->assertSame( 'label', $menu->getLabel() );
        $this->assertSame( [ 'route' => 'arg' ], $menu->getRouteArgs() );
        $this->assertSame( 'icon', $menu->getIcon() );
        $this->assertSame( 'badge', $menu->getBadge() );
        $this->assertSame( 'badgeColor', $menu->getBadgeColor() );
        
        $parentMenu = new MenuItem( 'idparent', 'labelparent', 'routeparent', [ 'route' => 'arg' ] );
        $parentMenu->addChild( $menu );
        $parentMenu->addChild( clone $menu );
        
        $this->assertEquals( $parentMenu, $menu->getParent() );
        $this->assertTrue( $parentMenu->hasChildren() );
        $this->assertCount( 2, $parentMenu->getChildren() );
    }
}
