<?php

namespace Silvioq\Component\Theme\Model\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Model\User;

class UserTest extends TestCase
{
    public function testUser()
    {
        $user = new User( 'uname', 'avatar', 'memberSince', true, 'name', 'title' );
        $this->assertSame( 'uname', $user->getUsername() );
        $this->assertSame( 'avatar', $user->getAvatar() );
        $this->assertSame( 'memberSince', $user->getMemberSince() );
        $this->assertSame( true, $user->getIsOnline() );
        $this->assertSame( 'name', $user->getName() );
        $this->assertSame( 'title', $user->getTitle() );
        
        $user
            ->setProfileRoute( 'proute' )
            ->setProfileRouteArgs( [ 'proute' => 'args' ] )
            ->setLogoutRoute( 'lroute' );
        $this->assertSame( 'proute', $user->getProfileRoute() );
        $this->assertSame( [ 'proute' => 'args' ], $user->getProfileRouteArgs() );
        $this->assertSame( 'lroute', $user->getLogoutRoute() );
    }

    public function testDefaultLogoutUserIsNull()
    {
        $user = new User( 'uname', 'avatar', 'memberSince', true, 'name', 'title' );
        $this->assertNull( $user->getLogoutRoute() );
    }
}

