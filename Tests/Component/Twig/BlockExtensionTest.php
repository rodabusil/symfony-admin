<?php

namespace Silvioq\Component\Theme\Twig\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Block\BlockBuilderInterface;
use Silvioq\Component\Theme\Block\BlockDeferredManager;
use Silvioq\Component\Theme\Twig\BlockExtension;
use  Symfony\Component\HttpFoundation\ParameterBag;

class BlockExtensionTest extends TestCase
{
    public function testTwigBlockExtensionCreation()
    {
        $mock = $this
            ->getMockBuilder(BlockBuilderInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $macro = new BlockExtension($mock, new ParameterBag());
        $globals = $macro->getGlobals();
        $this->assertInstanceOf( BlockDeferredManager::class, $globals['silvioq_block'] );
    }
}

