<?php

namespace Silvioq\Component\Theme\Twig\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Asset\Packages;
use Silvioq\Component\Theme\Twig\AssetThemeExtension;

class AssetThemeExtensionTest extends TestCase
{
    public function testCreation()
    {
        $mock = $this
            ->getMockBuilder(Packages::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->once())
            ->method('getUrl')
            ->with('theme/x')
            ->willReturn('xx')
            ;

        $ext = new AssetThemeExtension($mock, 'theme');
        $functions = $ext->getFunctions();
        $this->assertCount(1, $functions);
        $this->assertSame('silvioq.theme.asset.extension', $ext->getName() );
        $call = $functions[0]->getCallable();
        $this->assertSame( 'xx', $call('x'));
    }
}

