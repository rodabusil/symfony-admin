<?php

namespace Silvioq\Component\Theme\Twig\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Asset\Packages;
use Silvioq\Component\Theme\Twig\MacroExtension;
use Silvioq\Component\AdminTool\Asset\MacroAsset;

class MacroExtensionTest extends TestCase
{
    public function testCreation()
    {
        $mock = $this
            ->getMockBuilder(Packages::class)
            ->disableOriginalConstructor()
            ->getMock();

        $macro = new MacroExtension($mock);
        $globals = $macro->getGlobals();
        $this->assertInstanceOf( MacroAsset::class, $globals['silvioq_macro'] );
    }
}

