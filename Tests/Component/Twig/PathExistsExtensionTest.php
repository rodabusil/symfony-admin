<?php

namespace Silvioq\Component\Theme\Twig\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Asset\Packages;
use Silvioq\Component\Theme\Twig\MacroExtension;
use Silvioq\Component\AdminTool\Route\RouteCheckerInterface;
use Silvioq\Component\Theme\Twig\PathExistsExtension;

class PathExistsExtensionTest extends TestCase
{
    public function testTwigPathExistsExtensionCreation()
    {
        $mock = $this
            ->getMockBuilder(RouteCheckerInterface::class)
            ->disableOriginalConstructor()
            ->setMethods(['routeExists'])
            ->getMock();
        
        $mock->expects($this->once())
            ->method('routeExists')
            ->with('my_path')
            ->will($this->returnValue(true))
            ;
        
        $extension = new PathExistsExtension($mock);
        $functions = $extension->getFunctions();
        $this->assertInstanceOf( \Twig_SimpleFunction::class, $functions[0] );
        $this->assertSame( $functions[0]->getName(), 'path_exists' );
        $callable = $functions[0]->getCallable();
        $this->assertTrue($callable('my_path'));
    }
}
