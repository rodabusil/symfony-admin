<?php

namespace Silvioq\Component\AdminTool\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;

class ColorTypeTest extends TypeTestCase
{

    const TESTED_TYPE = 'Silvioq\Component\AdminTool\Form\ColorType';

    public function testColorFormType()
    {
        $form = $this->factory->create(self::TESTED_TYPE, 'name', [
            'empty_data' => '',
        ]);

        $form->submit(null);
        $this->assertSame('', $form->getData());
        $this->assertSame('', $form->getNormData());
        $this->assertSame('', $form->getViewData());
    }

    public function testDeprecation()
    {
        $deprecations = [];
        error_reporting(-1);
        set_error_handler(function ($type, $msg) use (&$deprecations) {
            if (E_USER_DEPRECATED === $type) {
                $deprecations[] = $msg;
            }
        });
        $form = $this->factory->create(\Silvioq\ThemeLTEBundle\Form\ColorType::class, new \DateTime(), [
        ]);
        $this->assertCount(1, $deprecations);
        $this->assertSame('Silvioq\ThemeLTEBundle\Form deprecated. Use Silvioq\Component\AdminTool\Form instead', $deprecations[0]);
    }

}

