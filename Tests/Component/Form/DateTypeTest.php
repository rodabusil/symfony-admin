<?php

namespace Silvioq\Component\AdminTool\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;

class DateTypeTest extends TypeTestCase
{

    const TESTED_TYPE = 'Silvioq\Component\AdminTool\Form\DateType';
    const DATETEST = '1986-06-22'; // The Goal of the Century

    public function testDateFormType()
    {
        $form = $this->factory->create(self::TESTED_TYPE, new \DateTime(), [
        ]);

        $form->submit(self::DATETEST);
        $this->assertSame(self::DATETEST, $form->getData()->format('Y-m-d'));
        $this->assertSame(self::DATETEST, $form->getNormData()->format('Y-m-d'));
        $this->assertSame(self::DATETEST, $form->getViewData());
    }
    
    public function testDefaultValue()
    {
        $form = $this->factory->create(self::TESTED_TYPE,  new \DateTime());
        $options = $form->getConfig()->getOptions();
        $this->assertEquals( 'single_text', $options['widget'] );
    }
    
}
