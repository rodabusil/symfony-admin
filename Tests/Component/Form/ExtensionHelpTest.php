<?php

namespace Silvioq\Component\AdminTool\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;
use Silvioq\Component\AdminTool\Form\ExtensionHelp;

class ExtensionHelpTest extends TypeTestCase
{
    const TESTED_TYPE = 'Symfony\Component\Form\Extension\Core\Type\TextType';
    
    public function testFormTypeExtensionHelpCreation()
    {
        $form = $this->factory->create(static::TESTED_TYPE, 'name', array(
            'empty_data' => '',
            'help' => 'Help text',
        ));
        
        $form->submit(null);
        $this->assertSame('', $form->getData());
        $this->assertSame('', $form->getNormData());
        $this->assertSame('', $form->getViewData());
        $options = $form->getConfig()->getOptions();
        $this->assertEquals( 'Help text', $options['help'] );
        
    }
    
    protected function getTypeExtensions()
    {
        return [ new ExtensionHelp() ];
    }
}
