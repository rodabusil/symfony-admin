<?php

namespace Silvioq\Component\AdminTool\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;

class TimeTypeTest extends TypeTestCase
{

    const TESTED_TYPE = 'Silvioq\Component\AdminTool\Form\TimeType';
    const DATETEST = '10:22';

    public function testTimeFormType()
    {
        $form = $this->factory->create(self::TESTED_TYPE, new \DateTime(), [
        ]);

        $form->submit(self::DATETEST);
        $this->assertSame(self::DATETEST, $form->getData()->format('H:i'));
        $this->assertSame(self::DATETEST, $form->getNormData()->format('H:i'));
        $this->assertSame(self::DATETEST, $form->getViewData());
    }
 
    public function testDefaultValue()
    {
        $form = $this->factory->create(self::TESTED_TYPE,  new \DateTime());
        $options = $form->getConfig()->getOptions();
        $this->assertEquals( 'single_text', $options['widget'] );
    }

}
