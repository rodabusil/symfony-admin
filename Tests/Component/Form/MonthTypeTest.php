<?php

namespace Silvioq\Component\AdminTool\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;
use Locale;

class MonthTypeTest extends TypeTestCase
{

    const TESTED_TYPE = 'Silvioq\Component\AdminTool\Form\MonthType';
    const MONTHTEST = 1;

    public function testMonthFormType()
    {
        $form = $this->factory->create(self::TESTED_TYPE, 1, []);

        $form->submit(self::MONTHTEST);
        $this->assertSame(self::MONTHTEST, $form->getData());
        $this->assertSame(self::MONTHTEST, (int)$form->getViewData());
    }
 
    public function testChoicesNames()
    {
        $oldLocale = Locale::getDefault();
        $this->assertTrue( !!$oldLocale, "Can't set locale for this installation" );
        Locale::setDefault("en");

        $form = $this->factory->create(self::TESTED_TYPE, 1);
        $options = $form->getConfig()->getOptions();

        $choices = $options['choices'];
        
        $this->assertTrue( is_array( $choices ) );
        $this->assertCount( 12, $choices );
        $this->assertSame( $choices['January'], 1 );
        $this->assertSame( $choices['February'], 2 );
    }

}
