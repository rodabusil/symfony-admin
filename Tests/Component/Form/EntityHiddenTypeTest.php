<?php

namespace Silvioq\Component\AdminTool\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;
use Silvioq\Component\AdminTool\Form\EntityHiddenType;
use Doctrine\Common\Persistence\ObjectManager;

class EntityHiddenTypeTest extends TypeTestCase
{

    const TESTED_TYPE = 'Silvioq\Component\AdminTool\Form\EntityHiddenType';
    
    public function testEntityTypeType()
    {
        $entity = $this
                ->getMockBuilder(\stdClass::class)
                ->setMethods(['getId'])
                ->getMock();
        $entity->expects($this->once())
            ->method('getId')
            ->will($this->returnValue(1))
            ;
        
        $form = $this->factory->create(self::TESTED_TYPE, $entity, [
            'data_class' => null,
            'class' => \stdClass::class,
        ]);

        $form->submit(1);
        $this->assertInstanceOf(\stdClass::class, $form->getData());
        $this->assertSame('1', $form->getNormData());
        $this->assertSame('1', $form->getViewData());
    }
    
    protected function getExtensions()
    {
        $mockEm = $this
                ->getMockBuilder(ObjectManager::class)
                ->disableOriginalConstructor()
                ->getMock();
        $mockRepo = $this
                ->getMockBuilder(\Doctrine\Common\Persistence\ObjectRepository::class)
                ->disableOriginalConstructor()
                ->getMock();
                
        $mockRepo->expects($this->any())
            ->method('find')
            ->will($this->returnValue(new \stdClass() ))
            ;
                
        $mockEm->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue($mockRepo))
            ;
        $entityHiddenType = new EntityHiddenType($mockEm);
        
        $mockExtension = $this
                ->getMockBuilder(\Symfony\Component\Form\AbstractExtension::class)
                ->disableOriginalConstructor()
                ->setMethods(['loadTypes'])
                ->getMock();
        $mockExtension->expects($this->once())
            ->method("loadTypes")
            ->will($this->returnValue([$entityHiddenType]));
        
        return [
            $mockExtension
        ];
    }

}
