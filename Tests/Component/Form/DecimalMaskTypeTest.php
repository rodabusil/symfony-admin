<?php

namespace Silvioq\Component\AdminTool\Tests\Form;

use Symfony\Component\Form\Test\TypeTestCase;

class DecimalMaskTypeTest extends TypeTestCase
{

    const TESTED_TYPE = 'Silvioq\Component\AdminTool\Form\DecimalMaskType';

    /**
     * @dataProvider getCheckData
     */
    public function testMoneyFormType($submittedData, $data, $normData, $viewData)
    {
        $form = $this->factory->create(self::TESTED_TYPE, null, [
            'grouping_symbol' => '.',
            'decimal_symbol' => ',',
        ]);

        $form->submit($submittedData);
        $this->assertSame($data, $form->getData());
        $this->assertSame($normData, $form->getNormData());
        $this->assertSame($viewData, $form->getViewData());
    }

    /**
     * @return array
     */
    public function getCheckData():array
    {
        return [
            [ null, null, null, '' ],
            [ '1,0', 1.0, 1.0, '1,00' ],
            [ '1,01', 1.01, 1.01, '1,01' ],
            [ '1341,01', 1341.01, 1341.01, '1.341,01' ],
            [ '1.341,01', 1341.01, 1341.01, '1.341,01' ],
        ];
    }

    public function testView()
    {
        $form = $this->factory->create(self::TESTED_TYPE, null, [
            'grouping_symbol' => 'g',
            'decimal_symbol' => 'd',
            'scale' => 6,
        ]);

        $view = $form->createView();
        $mask = $view->vars['mask'];
        $this->assertSame('g', $mask['grouping_symbol']);
        $this->assertSame('d', $mask['decimal_symbol']);
        $this->assertSame(6, $mask['scale']);
    }

    public function testViewLocale()
    {
        \Locale::setDefault('es');
        $form = $this->factory->create(self::TESTED_TYPE, null, [
        ]);

        $view = $form->createView();
        $mask = $view->vars['mask'];

        $this->assertSame("\u{00A0}", $mask['grouping_symbol']);
        $this->assertSame(',', $mask['decimal_symbol']);
        $this->assertSame(2, $mask['scale']);
    }
}
// vim:sw=4 ts=4 sts=4 et
