<?php

namespace Silvioq\Component\Theme\Block\Test;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Model\MenuItem;
use Silvioq\Component\Theme\Model\User;
use Silvioq\Component\Theme\Model\Notification;
use Silvioq\Component\Theme\Block\BlockManager;

class BlockManagerTest extends TestCase
{
    public function testBlockManager()
    {
        $mockMenu = $this->getMockBuilder(MenuItem::class)
                    ->disableOriginalConstructor()
                    ->getMock();
        
        $mockMenu->expects($this->exactly(2))
            ->method('getIdentifier')
            ->will($this->onConsecutiveCalls('id1','id2'));

        $mockSide = $this->getMockBuilder(MenuItem::class)
                    ->disableOriginalConstructor()
                    ->getMock();
        
        $mockSide->expects($this->exactly(2))
            ->method('getIdentifier')
            ->will($this->onConsecutiveCalls('side1','side2'));
        
        $mockUser = $this->getMockBuilder(User::class)
                    ->disableOriginalConstructor()
                    ->getMock();

        $mockUser->expects($this->once())
            ->method('getUsername')
            ->willReturn('user')
            ;
            
        $mockNot = $this->getMockBuilder(Notification::class)
                    ->disableOriginalConstructor()
                    ->getMock()
                    ;
                    
        $mockNot->expects($this->exactly(2))
            ->method('getMessage')
            ->will($this->onConsecutiveCalls('msg1','msg2'));
        
        $manager = new BlockManager( [ $mockMenu, $mockMenu ],
            [ $mockSide, $mockSide ],
            $mockUser,
            [ $mockNot, $mockNot ] );

        $this->assertEquals( $manager->getTopMenus()[0]->getIdentifier(), 'id1' );
        $this->assertEquals( $manager->getTopMenus()[1]->getIdentifier(), 'id2' );
        $this->assertEquals( $manager->getSideMenus()[0]->getIdentifier(), 'side1' );
        $this->assertEquals( $manager->getSideMenus()[1]->getIdentifier(), 'side2' );
        $this->assertEquals( $manager->getUser()->getUsername(), 'user' );
        $this->assertEquals( $manager->getNotifications()[0]->getMessage(), 'msg1' );
        $this->assertEquals( $manager->getNotifications()[1]->getMessage(), 'msg2' );
        
        $this->assertTrue( $manager->hasTopMenus() );
        $this->assertTrue( $manager->hasSideMenus() );
        $this->assertTrue( $manager->hasNotifications() );
    }

    public function testHasNotElementsInBlock()
    {
        $manager = new BlockManager( [ ], [ ], null, [] );
        $this->assertFalse( $manager->hasTopMenus() );
        $this->assertFalse( $manager->hasSideMenus() );
        $this->assertFalse( $manager->hasNotifications() );
    }
}
