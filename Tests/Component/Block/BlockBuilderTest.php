<?php

namespace Silvioq\Component\Theme\Block\Test;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Model\MenuItem;
use Silvioq\Component\Theme\Model\User;
use Silvioq\Component\Theme\Model\UserInterface;
use Silvioq\Component\Theme\Model\Notification;
use Silvioq\Component\Theme\Block\Provider\MenuProviderInterface;
use Silvioq\Component\Theme\Block\Provider\UserProviderInterface;
use Silvioq\Component\Theme\Block\Provider\NotificationProviderInterface;

use Silvioq\Component\Theme\Block\BlockManagerInterface;
use Silvioq\Component\Theme\Block\BlockBuilder;

class BlockBuilderTest extends TestCase
{
    public function testBlockBuilderMenuTop()
    {
        $mockMenu = $this->getMockBuilder(MenuItem::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getIdentifier"])
                    ->getMock();
        
        $mockMenu->expects($this->exactly(3))
            ->method('getIdentifier')
            ->will($this->onConsecutiveCalls('id1','id2', 'id3'));

        $mockTopMenuProvider = $this->getMockBuilder(MenuProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();

        $mockTopMenuProvider->expects($this->once())
            ->method('getItems')
            ->will($this->returnValue( [ $mockMenu ] ));

        $mockTopMenuProvider2 = $this->getMockBuilder(MenuProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();

        $mockTopMenuProvider2->expects($this->once())
            ->method('getItems')
            ->will($this->returnValue( [ $mockMenu, $mockMenu ] ) );
            
        $blockBuilder = new BlockBuilder();
        $blockBuilder
            ->addTopMenuProvider( $mockTopMenuProvider )
            ->addTopMenuProvider( $mockTopMenuProvider2 )
            ;
            
        $blockManager = $blockBuilder->build();
        $this->assertInstanceOf( BlockManagerInterface::class, $blockManager );
        $menus = $blockManager->getTopMenus();
        $this->assertCount( 3, $menus );
        $this->assertSame( 'id1', $menus[0]->getIdentifier() );
        $this->assertSame( 'id2', $menus[1]->getIdentifier() );
        $this->assertSame( 'id3', $menus[2]->getIdentifier() );
        
        $this->assertCount( 0, $blockManager->getSideMenus() );
        $this->assertCount( 0, $blockManager->getNotifications() );
        $this->assertNull( $blockManager->getUser() );
    }

    public function testBlockBuilderMenuSide()
    {
        $mockMenu = $this->getMockBuilder(MenuItem::class)
                    ->disableOriginalConstructor()
                    ->getMock();
        
        $mockMenu->expects($this->exactly(3))
            ->method('getIdentifier')
            ->will($this->onConsecutiveCalls('id1','id2', 'id3'));

        $mockMenuProvider = $this->getMockBuilder(MenuProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();

        $mockMenuProvider->expects($this->once())
            ->method('getItems')
            ->will($this->returnValue( [ $mockMenu ] ));

        $mockMenuProvider2 = $this->getMockBuilder(MenuProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();

        $mockMenuProvider2->expects($this->once())
            ->method('getItems')
            ->will($this->returnValue( [ $mockMenu, $mockMenu ] ) );
            
        $blockBuilder = new BlockBuilder();
        $blockBuilder
            ->addSideMenuProvider( $mockMenuProvider )
            ->addSideMenuProvider( $mockMenuProvider2 )
            ;
            
        $blockManager = $blockBuilder->build();
        $this->assertInstanceOf( BlockManagerInterface::class, $blockManager );
        $menus = $blockManager->getSideMenus();
        $this->assertCount( 3, $menus );
        $this->assertSame( 'id1', $menus[0]->getIdentifier() );
        $this->assertSame( 'id2', $menus[1]->getIdentifier() );
        $this->assertSame( 'id3', $menus[2]->getIdentifier() );
        
        $this->assertCount( 0, $blockManager->getTopMenus() );
        $this->assertCount( 0, $blockManager->getNotifications() );
        $this->assertNull( $blockManager->getUser() );
    }

    public function testBlockBuilderUser()
    {
        $mockUser = $this->getMockBuilder(User::class)
                    ->disableOriginalConstructor()
                    ->getMock();
        
        $mockUser->expects($this->exactly(1))
            ->method('getUsername')
            ->will($this->onConsecutiveCalls('user'));

        $mockUserProvider = $this->getMockBuilder(UserProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getUser","setUser"])
                    ->getMock();

        $mockUserProvider->expects($this->once())
            ->method('getUser')
            ->will($this->returnValue( $mockUser ));

        $mockUserProvider2 = $this->getMockBuilder(UserProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getUser","setUser"])
                    ->getMock();

        $mockUserProvider2->expects($this->never())
            ->method('getUser');
            
        $blockBuilder = new BlockBuilder();
        $blockBuilder
            ->addUserProvider( $mockUserProvider )
            ->addUserProvider( $mockUserProvider2 )
            ;
            
        $blockManager = $blockBuilder->build();
        $this->assertInstanceOf( BlockManagerInterface::class, $blockManager );
        $user = $blockManager->getUser();
        $this->assertInstanceOf( UserInterface::class, $user );
        $this->assertSame( $user->getUsername(), 'user');
        
        $this->assertCount( 0, $blockManager->getSideMenus() );
        $this->assertCount( 0, $blockManager->getTopMenus() );
        $this->assertCount( 0, $blockManager->getNotifications() );
        
    }


    public function testBlockBuilderNotification()
    {
        $mockNot = $this->getMockBuilder(Notification::class)
                    ->disableOriginalConstructor()
                    ->getMock();
        
        $mockNot->expects($this->exactly(3))
            ->method('getMessage')
            ->will($this->onConsecutiveCalls('msg1','msg2', 'msg3'));

        $mockNotProvider = $this->getMockBuilder(NotificationProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getNotifications","addNotification"])
                    ->getMock();

        $mockNotProvider->expects($this->once())
            ->method('getNotifications')
            ->will($this->returnValue( [ $mockNot ] ));

        $mockNotProvider2 = $this->getMockBuilder(NotificationProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getNotifications","addNotification"])
                    ->getMock();

        $mockNotProvider2->expects($this->once())
            ->method('getNotifications')
            ->will($this->returnValue( [ $mockNot, $mockNot ] ) );
            
        $blockBuilder = new BlockBuilder();
        $blockBuilder
            ->addNotificationProvider( $mockNotProvider )
            ->addNotificationProvider( $mockNotProvider2 )
            ;
            
        $blockManager = $blockBuilder->build();
        $this->assertInstanceOf( BlockManagerInterface::class, $blockManager );
        $nots = $blockManager->getNotifications();
        $this->assertCount( 3, $nots );
        $this->assertSame( 'msg1', $nots[0]->getMessage() );
        $this->assertSame( 'msg2', $nots[1]->getMessage() );
        $this->assertSame( 'msg3', $nots[2]->getMessage() );
        
        $this->assertCount( 0, $blockManager->getTopMenus() );
        $this->assertCount( 0, $blockManager->getSideMenus() );
        $this->assertNull( $blockManager->getUser() );
    }

}

