<?php

namespace Silvioq\ThemeLTEBundle\Form\Tests;

use Silvioq\ThemeLTEBundle\Form;

class DeprecatedClassesTest
{
    /**
     * @dataProvider getDeprecatedClases
     */
    public function testLTEFormDeprecations($class)
    {
        $deprecations = [];
        error_reporting(-1);
        set_error_handler(function ($type, $msg) use (&$deprecations) {
            if (E_USER_DEPRECATED === $type) {
                $deprecations[] = $msg;
            }
        });
        $form = $this->factory->create($class, null, [ ]);
        $this->assertCount(1, $deprecations);
        $this->assertSame('Silvioq\ThemeLTEBundle\Form deprecated. Use Silvioq\Component\AdminTool\Form instead', $deprecations[0]);
    }

    public function getDeprecatedClases()
    {
        return [
            [ Form\ColorType::class ],
            [ Form\DateType::class ],
            [ Form\EntityHiddenType::class ],
            [ Form\TimeType::class ],
            [ Form\ExtensionHelp::class ],
        ];
    }
}

// vim:sw=4 ts=4 sts=4 et
