<?php

namespace Silvioq\ThemeLTEBundle\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\ThemeLTEBundle\SilvioqThemeLTEBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BlockBuilderTest extends TestCase
{
    public function testIfDeprecatedMessageIsEmited()
    {
        $deprecations = [];
        error_reporting(-1);
        set_error_handler(function ($type, $msg) use (&$deprecations) {
            if (E_USER_DEPRECATED === $type) {
                $deprecations[] = $msg;
            }
        });
        new SilvioqThemeLTEBundle();
        $this->assertCount(1, $deprecations);
        $this->assertSame('Silvioq\ThemeLTEBundle\SilvioqThemeLTEBundle is deprectated. Use Silvioq\Bundle\ThemeBundle instead', $deprecations[0]);
    }
}
