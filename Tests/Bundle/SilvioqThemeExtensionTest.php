<?php

namespace Silvioq\Bundle\ThemeBundle\Tests;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;
use Silvioq\Component\Theme\Block\BlockBuilderInterface;

class SilvioqThemeExtensionTest extends AbstractExtensionTestCase
{
    protected function getContainerExtensions()
    {
        return array(
            new SilvioqThemeExtension()
        );
    }
    
    public function testAllServices()
    {
        $this->load();
        $this->compile();
        
        $this->assertContainerBuilderHasService("silvioq.theme.route.checker");
        $this->assertContainerBuilderHasServiceDefinitionWithTag("silvioq.theme.path_exists.extension", "twig.extension");
        $this->assertContainerBuilderHasServiceDefinitionWithTag("silvioq.theme.macro.extension", "twig.extension");
        $this->assertContainerBuilderHasServiceDefinitionWithTag("silvioq.theme.asset.extension", "twig.extension");
        $this->assertContainerBuilderHasServiceDefinitionWithTag("silvioq.theme.block.extension", "twig.extension");
        $this->assertContainerBuilderHasServiceDefinitionWithTag("silvioq.theme.twig.loader", "twig.loader", [ 'priority' => -100 ]);
        $this->assertContainerBuilderHasService("silvioq.form.extension.help");
        $this->assertContainerBuilderHasService("silvioq.theme.block.builder",\Silvioq\Component\Theme\Block\BlockBuilder::class);
        $this->assertInstanceOf( BlockBuilderInterface::class, $this->container->get("silvioq.theme.block.builder") );
        $this->assertContainerBuilderHasServiceDefinitionWithTag("silvioq.theme.install.command", "console.command");
    }
    
    public function testFormInclusion()
    {
        $this->setParameter('kernel.bundles', [ 'TwigBundle' => true ] );
        $this->setParameter('twig.form.resources', [ ] );
        $this->load();
        $this->assertContainerBuilderHasParameter('twig.form.resources');
        $this->assertEquals(
            $this->container->getParameter('twig.form.resources'),
            [ 'SilvioqThemeBundle:layout:form-theme.html.twig' ] );
    }
    
    /**
     * @expectedException \Symfony\Component\Config\Definition\Exception\InvalidConfigurationException
     * @expectedExceptionMessage The value "x" is not allowed for path "silvioq_theme.notifications.badge_type". Permissible values: "warning", "success", "danger", "info"
     */
    public function testInvalidConfiguration()
    {
        $this->load( [ 'notifications' => [ 'enabled' => true, 'badge_type' => 'x' ] ] );
    }

}

