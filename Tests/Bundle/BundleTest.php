<?php

namespace Silvioq\Bundle\ThemeBundle\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Bundle\ThemeBundle\SilvioqThemeBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class BlockBuilderTest extends TestCase
{
    public function testBundleConstructionForThemeBundle()
    {
        $this->assertNotNull( new SilvioqThemeBundle() );
    }
    
    public function testBuildSilvioqThemeBundle()
    {
        $bundle = new SilvioqThemeBundle();
        $container = new ContainerBuilder();
        $container->registerExtension($bundle->getContainerExtension());

        $bundle->build( $container );
        foreach( $container->getExtensions() as $e ) $e->load([], $container);
        $builder = $container->get('silvioq.theme.block.builder');
        $this->assertNotNull( $builder );
    }
}
