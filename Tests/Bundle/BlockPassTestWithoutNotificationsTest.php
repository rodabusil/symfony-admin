<?php

namespace Silvioq\Bundle\ThemeBundle\DependencyInjection\Compiler\Tests;

use Silvioq\Bundle\ThemeBundle\DependencyInjection\Compiler\AddBlockPass;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Silvioq\Component\Theme\Block\BlockBuilder;
use Silvioq\Component\Theme\Block\Provider\NotificationProviderInterface;
use Silvioq\Component\Theme\Block\Provider\UserProviderInterface;
use Symfony\Component\DependencyInjection\Reference;

class BlockPassTestWithoutNotificationsTest extends AbstractCompilerPassTestCase
{
    protected function registerCompilerPass(ContainerBuilder $container)
    {
        $container->addCompilerPass(new AddBlockPass());
    }

    public function testTaggedService()
    {
        $collectingService = $this->registerService('silvioq.theme.block.builder', BlockBuilder::class);

        $mockNot = $this->getMockBuilder(NotificationProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getNotifications","addNotification"])
                    ->getMock();

        $mockUser = $this->getMockBuilder(UserProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getUser","setUser"])
                    ->getMock();
        $tagService2 = $this->registerService('tagged.service.user', $mockUser)
                ->addTag('silvioq.theme.block', [ 'type' => 'user' ] )
                ;
        $tagService3 = $this->registerService('tagged.service.not', $mockNot)
                ->addTag('silvioq.theme.block', [ 'type' => 'notification' ] )
                ;

        $this->compile();
        $this->assertContainerBuilderHasService('silvioq.theme.block.builder');
        $this->assertCount(1, $this->container->getDefinition('silvioq.theme.block.builder')->getMethodCalls());

        $builder = $this->container->get('silvioq.theme.block.builder');
    }
    

}
// vim:sw=4 ts=4 sts=4 et
