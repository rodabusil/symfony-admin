<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Loader\Tests;

use Silvioq\Bundle\ThemeBundle\Twig\Loader\AlternativeThemeLoader;
use PHPUnit\Framework\TestCase;
use Twig\Loader\FilesystemLoader;

class AlternativeThemeLoaderTest extends TestCase
{
    const THEME_NAME = 'test-theme';

    public function testFileNotExists()
    {
        $mockLoader = $this->buildMockLoader([__DIR__.'/../Fixtures/Resources/views']);
        $alternativeLoader = new AlternativeThemeLoader( $mockLoader, self::THEME_NAME );

        $mockLoader->expects($this->never())
            ->method('exists')
            ;

        $this->assertFalse( $alternativeLoader->exists('/tmp/index.html.twig'));
    }

    public function testMainNamespace()
    {
        $mockLoader = $this->buildMockLoader([__DIR__.'/../Fixtures/Resources/views']);
        $originalName = '::base.html.twig';
        $alternativeLoader = new AlternativeThemeLoader( $mockLoader, self::THEME_NAME );
        $this->assertTrue( $alternativeLoader->exists( '::base.html.twig' ) );
    }
    
    public function testFunctionalCalling()
    {
        $mockLoader = $this->buildMockLoader([__DIR__.'/../Fixtures/Resources/views'], "My");
        $alternativeLoader = new AlternativeThemeLoader( $mockLoader, self::THEME_NAME );
        $originalName = 'MyBundle:default:index.html.twig';
        $name = '@My/default/index.html.twig';

        $mockLoader->expects($this->never())
            ->method('exists')
            ;

        $this->assertTrue( $alternativeLoader->exists($originalName));
        $this->assertFalse( $alternativeLoader->exists('MyBundle:default:index2.html.twig'));

        $time = (new \DateTime())->format('U') + 100;
        $this->assertTrue( $alternativeLoader->isFresh($originalName, $time ));

        $file = realpath( __DIR__.'/../Fixtures/Resources/views/' . self::THEME_NAME . '/default/index.html.twig' ); 
        $this->assertSame( $file, $alternativeLoader->getCacheKey($originalName));

        $source = $alternativeLoader->getSourceContext($originalName);
        $this->assertSame( $name, $source->getName());
        $this->assertSame( 'Hello World', trim($source->getCode()));

        $reflection = new \ReflectionClass(\Twig_Loader_Filesystem::class);
        if( $reflection->hasMethod('getSource') )
            $this->assertSame( 'Hello World', trim($alternativeLoader->getSource($originalName)));
    }

    public function testFunctionalCallinWithTwoThemes()
    {
        $mockLoader = $this->buildMockLoader([__DIR__.'/../Fixtures/Resources/views'], "My");
        $alternativeLoader = new AlternativeThemeLoader( $mockLoader, [ self::THEME_NAME, self::THEME_NAME . '2' ] );
        $originalName = 'MyBundle:default:index.html.twig';
        $originalName2 = 'MyBundle:default:index2.html.twig';
        $this->assertTrue( $alternativeLoader->exists($originalName));
        $this->assertTrue( $alternativeLoader->exists($originalName2));

        $source = $alternativeLoader->getSourceContext($originalName);
        $source2 = $alternativeLoader->getSourceContext($originalName2);
        $this->assertSame( 'Hello World', trim($source->getCode()));
        $this->assertSame( 'test2', trim($source2->getCode()));
        
        $file = realpath( __DIR__.'/../Fixtures/Resources/views/' . self::THEME_NAME . '2/default/index2.html.twig' ); 
        $this->assertSame( $file, $alternativeLoader->getCacheKey($originalName2));
    }

    private function buildMockLoader($paths = [], $ns = null)
    {
        if( null === $ns ) $ns = '__main__';

        /** @var FilesystemLoader */
        $mock = $this
                ->getMockBuilder(FilesystemLoader::class)
                ->setMethods( [ 'getSource', 'exists', 'isFresh', 'getCacheKey', 'getSourceContext', 'getNamespaces', 'getPaths' ] )
                ->disableOriginalConstructor()
                ->getMock()
                ;

        $mock->expects($this->once())
            ->method('getNamespaces')
            ->willReturn([$ns])
            ;
        $mock->expects($this->once())
            ->method('getPaths')
            ->with($ns)
            ->willReturn($paths)
            ;

        return $mock;
    }
}

