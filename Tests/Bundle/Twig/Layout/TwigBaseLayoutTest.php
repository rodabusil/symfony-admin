<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Layout\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Twig\AssetThemeExtension;
use Symfony\Component\Asset\Packages;
use Silvioq\Bundle\ThemeBundle\Twig\Loader\AlternativeThemeLoader;
use Silvioq\Component\Theme\Twig\BlockExtension;
use Silvioq\Component\Theme\Block\BlockBuilder;
use Symfony\Component\HttpFoundation\ParameterBag;
use Silvioq\Component\Theme\Model\MenuItem;
use Silvioq\Component\Theme\Block\Provider\MenuProviderInterface;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\TranslatorInterface;

class TwigBaseLayoutTest extends TestCase
{

    public function testBaseLayoutExample()
    {
        $twig = $this->buildTwig();
        $template = $twig->loadTemplate( 'adminLTE/layout/base-layout.html.twig' );
        $result = $template->render([]);
        $this->assertNotNull( $result );
        $this->assertRegExp( '@<h4><i class="icon fa fa-check"></i>Success!</h4>@', $result );
        $this->assertRegExp( '@<!-- sidebar menu: empty -->@', $result );
    }

    private function getAssetThemeExtension()
    {
        $mock = $this->getMockBuilder(Packages::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('getUrl')
            ->will($this->returnArgument(0))
            ;
        return new AssetThemeExtension($mock, "adminLTE");
        
    }

    private function buildTwig()
    {
        $originalLoader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views');
        $originalLoader->addPath(
                __DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views', 'SilvioqTheme'
            );
        $alternativeLoader = new AlternativeThemeLoader( $originalLoader, 'adminLTE' );
        $loader = new \Twig_Loader_Chain( [ $originalLoader, $alternativeLoader ] );

        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension($this->getAssetThemeExtension());
        $twig->addExtension($this->getBlockExtension());
        $twig->addExtension( $this->getTranslationExtension() );
        $twig->addFunction( new \Twig_SimpleFunction('asset', function($file){
                return "web/" . $file;
            } ) );
        $twig->addFunction( new \Twig_SimpleFunction('path', function($path){
                return "http://localhost/" . $path;
            } ) );
        $twig->addFunction( new \Twig_SimpleFunction('is_granted', function(){
                return true;
            }) );
            
            
        $flashbag = new ParameterBag( [
            'success' => [ 'Message success', 'Message success 2' ],
            'warning' => [ 'Message warning', 'Message warning 2' ],
            'error' => [ 'Message error', 'Message error 2' ],
            'info' => [ 'Message info', 'Message info 2' ],
        ] );

        $twig->addGlobal( 'app', [
                'user' => [
                ],
                'session' => [
                    'flashbag' => $flashbag,
                ],
           ] );
        return $twig;
    }

    private function getBlockExtension( $menu = null )
    {
        $parameterBag = new ParameterBag( [] );
        $builder = new BlockBuilder();
        
        $menuProvider = $this->getMockBuilder(MenuProviderInterface::class)
            ->getMock();
        
        if( null === $menu )
            $menu = [];

        $menuProvider->expects($this->once())
            ->method('getItems')
            ->willReturn($menu);
            ;

        $builder->addSideMenuProvider( $menuProvider );
        return new BlockExtension( $builder, $parameterBag );
    }

    private function getTranslationExtension()
    {
        return new TranslationExtension( $this->getMockTranslator() );
    }

    private function getMockTranslator()
    {
        $mock = $this->getMockBuilder(TranslatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('trans')
            ->will($this->returnArgument(0))
            ;
        return $mock;
    }


}
// vim:sw=4 ts=4 sts=4 et

