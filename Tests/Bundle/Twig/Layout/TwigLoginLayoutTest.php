<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Layout\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Component\Theme\Twig\AssetThemeExtension;
use Symfony\Component\Asset\Packages;

class TwigLoginLayoutTest extends TestCase
{

    public function testLoginLayoutTemplate()
    {
        $twig = $this->buildTwig();
        $template = $twig->loadTemplate( 'adminLTE/layout/login-layout.html.twig' );
        $result = $template->render([]);
        $expects = <<<EOF
<!doctype html>
<!--[if lt IE 7 ]><html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]><html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]><html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]><html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html lang="en" class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Silvioq Admin!</title>

        <link rel="stylesheet" href="adminLTE/css/AdminLTE.min.css" />
        <link rel="stylesheet" href="adminLTE/bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" href="adminLTE/css/skins/skin-blue.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="icon" type="image/x-icon" href="web/favicon.ico" />

            
</head>
<body class="login-page">

    <script scr="adminLTE/plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
    <script src="adminLTE/js/app.min.js" type="text/javascript"></script>
    <script src="adminLTE/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

</body>
</html>
EOF
;
        $this->assertSame( $expects, trim($result) );
    }

    private function getAssetThemeExtension()
    {
        $mock = $this->getMockBuilder(Packages::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('getUrl')
            ->will($this->returnArgument(0))
            ;
        return new AssetThemeExtension($mock, "adminLTE");
        
    }

    private function buildTwig()
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views' );
        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension($this->getAssetThemeExtension());
        $twig->addFunction( new \Twig_SimpleFunction('asset', function($file){
                return "web/" . $file;
            } ) );

        return $twig;
    }

}
// vim:sw=4 ts=4 sts=4 et
