<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Block\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\TranslatorInterface;
use Silvioq\Component\Theme\Twig\BlockExtension;
use Symfony\Component\HttpFoundation\ParameterBag;
use Silvioq\Component\Theme\Block\BlockBuilder;
use Silvioq\Component\Theme\Model\Notification;
use Silvioq\Component\Theme\Block\Provider\NotificationProviderInterface;

class TwigNotificationBlockTest extends TestCase
{

    public function testRenderNotificationDisabledBlock()
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views' );
        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension( $this->getTranslationExtension() );
        $twig->addExtension( $this->getBlockExtension() );
        $template = $twig->loadTemplate( 'adminLTE/block/notifications.html.twig' );
        $result = $template->render([]);
        $this->assertSame( '<!-- notifications disabled -->', $result );
    }

    public function testRenderNotificationEmptyBlock()
    {
        $params = [ 'notificationsEnabled' => true ];
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views' );
        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension( $this->getTranslationExtension() );
        $twig->addExtension( $this->getBlockExtension($params) );
        $template = $twig->loadTemplate( 'adminLTE/block/notifications.html.twig' );
        $result = $template->render([]);
        $this->assertSame( '<!-- Notifications: style can be found in dropdown.less -->', trim($result) );
    }

    public function testRenderNotificationBlock()
    {
        $params = [ 'notificationsEnabled' => true, "viewAllNotificationsUrl" => "url" ];
        $msgs = [ 'mensaje 1', 'mensaje 2'];
        
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views' );
        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension( $this->getTranslationExtension() );
        $twig->addExtension( $this->getBlockExtension($params, $msgs) );
        $template = $twig->loadTemplate( 'adminLTE/block/notifications.html.twig' );
        $result = $template->render([]);
        
        $expected = <<<EOS
<!-- Notifications: style can be found in dropdown.less -->
<li class="dropdown notifications-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-bell-o"></i>
        <span class="label label-warning">2</span>
    </a>
    <ul class="dropdown-menu">
        <li class="header"></li>
        <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
                <li>
                    <a href="">
                        <i class="fa fa-warning info"></i> mensaje 1
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="fa fa-warning info"></i> mensaje 2
                    </a>
                </li>
            </ul>
        </li>
        <li class="footer"><a href="url">View all</a></li>
    </ul>
</li>
EOS
;
        $this->assertSame( $expected, trim($result) );
    }

    private function getBlockExtension( $config = [], $notificationMessages = [] )
    {
        $parameterBag = new ParameterBag( $config );
        $builder = new BlockBuilder();
        
        $notifProvider = $this->getMockBuilder(NotificationProviderInterface::class)
            ->getMock();
        $nots = [];
        foreach( $notificationMessages as $msg )
        {
            $nots[] = new Notification( $msg );
        }
        $notifProvider->expects($this->any())
            ->method('getNotifications')
            ->willReturn($nots);
            ;

        $builder->addNotificationProvider( $notifProvider );
        return new BlockExtension( $builder, $parameterBag );
    }
    
    private function getTranslationExtension()
    {
        return new TranslationExtension( $this->getMockTranslator() );
    }
    
    /**
     * @return TranslatorInterface
     */
    private function getMockTranslator()
    {
        $mock = $this->getMockBuilder(TranslatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('trans')
            ->will($this->returnArgument(0))
            ;
        return $mock;
    }
}
// vim:sw=4 ts=4 sts=4 et
