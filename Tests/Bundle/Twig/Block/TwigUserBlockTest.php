<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Block\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\TranslatorInterface;
use Silvioq\Component\Theme\Twig\BlockExtension;
use Symfony\Component\HttpFoundation\ParameterBag;
use Silvioq\Component\Theme\Block\BlockBuilder;
use Silvioq\Component\Theme\Model\User;
use Silvioq\Component\Theme\Block\Provider\UserProviderInterface;

class TwigUserBlockTest extends TestCase
{

    public function testRenderUserEmptyBlock()
    {
        $twig = $this->buildTwig();
        $template = $twig->loadTemplate( 'adminLTE/block/topUser.html.twig' );
        $result = $template->render([]);
        $this->assertSame( '<!-- no user data -->', trim($result) );
    }

    public function testRendeUserBlock()
    {
        $twig = $this->buildTwig("User name");
        $template = $twig->loadTemplate( 'adminLTE/block/topUser.html.twig' );
        $result = $template->render([]);

        $expected = <<<EOS
<!-- User Account: style can be found in dropdown.less -->
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <img src="web/http://gravatar.com/img.jpg" class="user-image" alt="User name" />
        <span class="hidden-xs">Real User name</span>
    </a>
    <ul class="dropdown-menu">
        <!-- User image -->
        <li class="user-header">
            <img src="web/http://gravatar.com/img.jpg" class="img-circle" alt="User name" />
            <p>
                Real User name
                <small>Member since %date%</small>
            </p>
        </li>
        <!-- Menu Body -->
                <!-- Menu Footer-->
                <li class="user-footer">
            <div class="pull-left">
                <a href="http://localhost/" class="btn btn-default btn-flat">
                    Profile
                 </a>
            </div>
            <div class="pull-right">
                <a href="http://localhost/" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>
</li>
EOS
;
        $this->assertSame( $expected, trim($result) );
    }

    private function getBlockExtension( $username = null )
    {
        $parameterBag = new ParameterBag( [] );
        $builder = new BlockBuilder();
        
        $userProvider = $this->getMockBuilder(UserProviderInterface::class)
            ->getMock();
        
        if( $username )
            $user = new User( $username, "http://gravatar.com/img.jpg", new \DateTime( '1986-06-22' ), true, "Real " . $username );
        else
            $user = null;

        $userProvider->expects($this->once())
            ->method('getUser')
            ->willReturn($user);
            ;

        $builder->addUserProvider( $userProvider );
        return new BlockExtension( $builder, $parameterBag );
    }
    
    private function getTranslationExtension()
    {
        return new TranslationExtension( $this->getMockTranslator() );
    }
    
    private function getMockTranslator()
    {
        $mock = $this->getMockBuilder(TranslatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('trans')
            ->will($this->returnArgument(0))
            ;
        return $mock;
    }

    private function buildTwig($username = null)
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views' );
        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension( $this->getTranslationExtension() );
        $twig->addExtension( $this->getBlockExtension($username) );
        $twig->addFunction( new \Twig_SimpleFunction('asset', function($file){
                return "web/" . $file;
            } ) );
        $twig->addFunction( new \Twig_SimpleFunction('path', function($path){
                return "http://localhost/" . $path;
            } ) );
        return $twig;
    }
}
// vim:sw=4 ts=4 sts=4 et
