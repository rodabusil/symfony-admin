<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Block\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\TranslatorInterface;
use Silvioq\Component\Theme\Twig\BlockExtension;
use Symfony\Component\HttpFoundation\ParameterBag;
use Silvioq\Component\Theme\Block\BlockBuilder;
use Silvioq\Component\Theme\Model\MenuItem;
use Silvioq\Component\Theme\Block\Provider\MenuProviderInterface;

class TwigSideBlockTest extends TestCase
{

    public function testRenderSideMenuEmptyBlock()
    {
        $twig = $this->buildTwig();
        $template = $twig->loadTemplate( 'adminLTE/block/sideMenu.html.twig' );
        $result = $template->render([]);
        $this->assertSame( '<!-- sidebar menu: empty -->', trim($result) );
    }

    /** 
     * @dataProvider getMenus
     */
    public function testRenderMenuBlock($menu, $expects)
    {
        $twig = $this->buildTwig($menu);
        $template = $twig->loadTemplate( 'adminLTE/block/sideMenu.html.twig' );
        $result = $template->render([]);

        if( null === $expects )
            print $result . PHP_EOL;
        else
            $this->assertSame( $expects, trim($result) );
    }

    public function getMenus()
    {
        $menu1 = [
            new MenuItem( "menu-id1", "label-1", "/route-1" ),
            new MenuItem( "menu-id2", "label-2", "/route-2" ),
        ];
        $expected1 = <<<EOS
<!-- sidebar menu: style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li id="menu-id1" class="">
        <a href="http://localhost//route-1">
            <i class="fa fa-info-o"></i><span>label-1</span>
        </a>
    </li>
    <li id="menu-id2" class="">
        <a href="http://localhost//route-2">
            <span>label-2</span>
        </a>
    </li>
</ul>
EOS
;

        $menu1[0]->setIcon( "fa fa-info-o");


        $menu2 = [
            new MenuItem( "menu-id1", "label-1", "/route-1" ),
            new MenuItem( "menu-id2", "label-2", "/route-2" ),
        ];
        $menu2[1]->setBadge( 'warning' );
        $menu22 = new MenuItem( "menu-id22", "label-22", "/route-22" );
        $menu22->setIcon( 'fa fa-info-o');
        $menu2[1]->addChild( new MenuItem( "menu-id21", "label-21", "/route-21" ) );
        $menu2[1]->addChild( $menu22 );

        $expected2 = <<<EOS
<!-- sidebar menu: style can be found in sidebar.less -->
<ul class="sidebar-menu">
    <li id="menu-id1" class="">
        <a href="http://localhost//route-1">
            <span>label-1</span>
        </a>
    </li>
    <li id="menu-id2" class=" treeview">
        <a href="#">
            <span>label-2</span><span class="pull-right-container"><small class="label pull-right bg-green">warning</small></span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li class="" id="menu-id21">
                <a href="http://localhost//route-21">
                <i class="fa fa-circle-o"></i>
                    label-21
                </a>
            </li>
            <li class="" id="menu-id22">
                <a href="http://localhost//route-22">
                <i class="fa fa-info-o"></i>
                    label-22
                </a>
            </li>
        </ul>
    </li>
</ul>
EOS
;

        return [ 
            [ $menu1, $expected1 ],
            [ $menu2, $expected2 ],
        ];
    }

    private function getBlockExtension( $menu = null )
    {
        $parameterBag = new ParameterBag( [] );
        $builder = new BlockBuilder();
        
        $menuProvider = $this->getMockBuilder(MenuProviderInterface::class)
            ->getMock();
        
        if( null === $menu )
            $menu = [];

        $menuProvider->expects($this->once())
            ->method('getItems')
            ->willReturn($menu);
            ;

        $builder->addSideMenuProvider( $menuProvider );
        return new BlockExtension( $builder, $parameterBag );
    }

    private function getTranslationExtension()
    {
        return new TranslationExtension( $this->getMockTranslator() );
    }
    
    private function getMockTranslator()
    {
        $mock = $this->getMockBuilder(TranslatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('trans')
            ->will($this->returnArgument(0))
            ;
        return $mock;
    }

    private function buildTwig($menu = null)
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views' );
        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension( $this->getTranslationExtension() );
        $twig->addExtension( $this->getBlockExtension($menu) );
        $twig->addFunction( new \Twig_SimpleFunction('asset', function($file){
                return "web/" . $file;
            } ) );
        $twig->addFunction( new \Twig_SimpleFunction('path', function($path){
                return "http://localhost/" . $path;
            } ) );
        return $twig;
    }
}
// vim:sw=4 ts=4 sts=4 et
