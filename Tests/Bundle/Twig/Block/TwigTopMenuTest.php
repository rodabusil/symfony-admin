<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Block\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\TranslatorInterface;
use Silvioq\Component\Theme\Twig\BlockExtension;
use Symfony\Component\HttpFoundation\ParameterBag;
use Silvioq\Component\Theme\Block\BlockBuilder;
use Silvioq\Component\Theme\Model\MenuItem;
use Silvioq\Component\Theme\Block\Provider\MenuProviderInterface;

class TwigTopMenuBlockTest extends TestCase
{

    public function testRenderSideMenuEmptyBlock()
    {
        $twig = $this->buildTwig();
        $template = $twig->loadTemplate( 'adminLTE/block/topMenu.html.twig' );
        $result = $template->render([]);
        $this->assertSame( '<!-- top menu empty -->', trim($result) );
    }

    /** 
     * @dataProvider getMenus
     */
    public function testRenderMenuBlock($menu, $expects)
    {
        $this->markTestIncomplete();
    }

    public function getMenus()
    {
        $menu1 = [
            new MenuItem( "menu-id1", "label-1", "/route-1" ),
            new MenuItem( "menu-id2", "label-2", "/route-2" ),
        ];
        $expected1 = <<<EOS
EOS
;

        $menu1[0]->setIcon( "fa fa-info-o");
        $menu2 = [
            new MenuItem( "menu-id1", "label-1", "/route-1" ),
            new MenuItem( "menu-id2", "label-2", "/route-2" ),
        ];
        $menu2[1]->setBadge( 'warning' );
        $menu22 = new MenuItem( "menu-id22", "label-22", "/route-22" );
        $menu22->setIcon( 'fa fa-info-o');
        $menu2[1]->addChild( new MenuItem( "menu-id21", "label-21", "/route-21" ) );
        $menu2[1]->addChild( $menu22 );

        $expected2 = <<<EOS
EOS
;

        return [ 
            [ $menu1, $expected1 ],
            [ $menu2, $expected2 ],
        ];
    }

    private function getBlockExtension( $menu = null )
    {
        $parameterBag = new ParameterBag( [] );
        $builder = new BlockBuilder();
        
        $menuProvider = $this->getMockBuilder(MenuProviderInterface::class)
            ->getMock();
        
        if( null === $menu )
            $menu = [];

        $menuProvider->expects($this->once())
            ->method('getItems')
            ->willReturn($menu);
            ;

        $builder->addSideMenuProvider( $menuProvider );
        return new BlockExtension( $builder, $parameterBag );
    }

    private function getTranslationExtension()
    {
        return new TranslationExtension( $this->getMockTranslator() );
    }

    private function getMockTranslator()
    {
        $mock = $this->getMockBuilder(TranslatorInterface::class)
            ->disableOriginalConstructor()
            ->getMock();
        $mock->expects($this->any())
            ->method('trans')
            ->will($this->returnArgument(0))
            ;
        return $mock;
    }

    private function buildTwig($menu = null)
    {
        $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../../../../src/Silvioq/Bundle/ThemeBundle/Resources/views' );
        $twig = new \Twig_Environment( $loader, ["strict_variables" => true] );
        $twig->addExtension( $this->getTranslationExtension() );
        $twig->addExtension( $this->getBlockExtension($menu) );
        $twig->addFunction( new \Twig_SimpleFunction('asset', function($file){
                return "web/" . $file;
            } ) );
        $twig->addFunction( new \Twig_SimpleFunction('path', function($path){
                return "http://localhost/" . $path;
            } ) );
        return $twig;
    }
}
// vim:sw=4 ts=4 sts=4 et
