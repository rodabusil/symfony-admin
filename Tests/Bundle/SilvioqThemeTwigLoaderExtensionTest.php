<?php

namespace Silvioq\Bundle\ThemeBundle\Twig\Loader\Tests;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;
use Silvioq\Bundle\ThemeBundle\Twig\Loader\AlternativeThemeLoader;
use Twig\Loader\FilesystemLoader;

class SilvioqThemeTwigExtensionTest extends AbstractExtensionTestCase
{
    protected function getContainerExtensions()
    {
        return array(
            new SilvioqThemeExtension()
        );
    }
    
    public function testAlternativeThemeLoaderArgument()
    {
        $this->load();
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
                "silvioq.theme.twig.loader", 1, ["adminLTE"]);
    }

    public function testExistsFile()
    {
        /** @var FilesystemLoader */
        $mock = $this
                ->getMockBuilder(FilesystemLoader::class)
                ->disableOriginalConstructor()
                ->getMock();

        $this->registerService('twig.loader.native_filesystem', $mock);

        $this->load( [ 'theme' => 'test-theme' ]);
        $originaLoader = $this->container->get("twig.loader.native_filesystem");
        $originaLoader->expects($this->once())
            ->method('getNamespaces')
            ->willReturn(['My'])
            ;
        $originaLoader->expects($this->once())
            ->method('getPaths')
            ->with('My')
            ->willReturn( [ __DIR__ . '/Fixtures/Resources/views' ] )
            ;

        $alternativeLoader = $this->container->get("silvioq.theme.twig.loader");
        $this->assertFalse( $alternativeLoader->exists( '/tmp/file' ) );
        $this->assertFalse( $alternativeLoader->exists( 'MyBundle:default:archive' ) );
        $this->assertTrue( $alternativeLoader->exists( '@My/default/index.html.twig' ) );
        $this->assertTrue( $alternativeLoader->exists( 'MyBundle:default:index.html.twig' ) );
    }
}
