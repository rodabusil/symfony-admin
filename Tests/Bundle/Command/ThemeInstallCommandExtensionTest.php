<?php
namespace  Silvioq\Bundle\ThemeBundle\Command\Tests;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;

class ThemeInstallCommandExtensionTest extends AbstractExtensionTestCase
{
    protected function getContainerExtensions()
    {
        return array(
            new SilvioqThemeExtension()
        );
    }
    
    public function testThemeInstallCommandConfiguration()
    {
        $this->setParameter( 'kernel.root_dir', __DIR__ );

        $this->load( [
            'web_path' => 'myweb',
            'theme' => 'mytheme',
        ] );

        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            'silvioq.theme.install.command',
            0, 'mytheme' );
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            'silvioq.theme.install.command',
            1, 'myweb' );
        $this->assertContainerBuilderHasServiceDefinitionWithArgument(
            'silvioq.theme.install.command',
            2, '%kernel.root_dir%/../web' );

        $this->assertContainerBuilderHasServiceDefinitionWithTag(
            'silvioq.theme.install.command', 'console.command'
          );

    }
    
    public function testLoadThemeInstallCommand()
    {
        $this->setParameter( 'kernel.root_dir', __DIR__ );
        
            $this->load( [
                'web_path' => 'myweb',
                'theme' => 'mytheme',
            ] );
            
        $this->assertNotNull($this->container->get('silvioq.theme.install.command'));
    }
}

