<?php
namespace  Silvioq\Bundle\ThemeBundle\Command\Tests;

use PHPUnit\Framework\TestCase;
use Silvioq\Bundle\ThemeBundle\Command\ThemeInstallCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;


class ThemeInstallCommandTest extends TestCase
{
    const THEME = "test-theme";
    const OUTPUT_DIR = __NAMESPACE__ . "/themes/recursive";
    const ROOT_PATH = "/tmp";
    
    public function testExecuteThemeInstallCommand()
    {
        $app = new Application("test", "1.0");
        $app->add( new ThemeInstallCommand( null, null, self::ROOT_PATH, __DIR__ . "/../Fixtures/Resources" ) );
        
        
        $command = $app->find( "silvioq:theme:install" );
        $this->assertInstanceOf( ThemeInstallCommand::class, $command );
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            '--output-dir' => self::OUTPUT_DIR,
            'theme' => self::THEME,
         ) );

        $this->assertEquals( 0, $commandTester->getStatusCode() );

        $output = $commandTester->getDisplay();
        $this->assertTrue( false !== strpos( $output, '[OK] Files copied: 9' ) );
        $this->assertTrue( is_dir( self::ROOT_PATH . '/' . self::OUTPUT_DIR ) );
        $this->assertTrue( is_readable( self::ROOT_PATH . '/' . self::OUTPUT_DIR . '/bootstrap/js/bootstrap.min.js' ) );
        
        $it = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(self::ROOT_PATH . '/' . self::OUTPUT_DIR));
        $it->rewind();

        $result = [];
        while($it->valid()) {
            if (!$it->isDot())
                $result[$it->getSubPathName()] = filesize( $it->key() );
            $it->next();
        }
        
        $this->assertEquals( [
                'jQuery/jquery-2.2.3.min.js' => 85659,
                'jQuery/jQuery-2.1.4.min.js' => 84345,
                'bootstrap/fonts/glyphicons-halflings-regular.woff2' => 18028,
                'bootstrap/fonts/glyphicons-halflings-regular.ttf' => 45404,
                'bootstrap/fonts/glyphicons-halflings-regular.svg' => 108738,
                'bootstrap/fonts/glyphicons-halflings-regular.woff' => 23424,
                'bootstrap/fonts/glyphicons-halflings-regular.eot' => 20127,
                'bootstrap/js/bootstrap.min.js' => 36868,
                'bootstrap/css/bootstrap.min.css' => 121260,
        ], $result );
    }
    
    public function testFailIfThemeNotExists()
    {
        $app = new Application("test", "1.0");
        $app->add( new ThemeInstallCommand( null, null, self::ROOT_PATH, __DIR__ . "/../Fixtures/Resources" ) );
        
        
        $command = $app->find( "silvioq:theme:install" );
        $this->assertInstanceOf( ThemeInstallCommand::class, $command );
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            '--output-dir' => self::OUTPUT_DIR,
            'theme' => 'inexistente',
         ) );
        
        $output = $commandTester->getDisplay();
        $this->assertEquals( 255, $commandTester->getStatusCode() );
        $this->assertTrue( false !== strpos( $output, 'Is inexistente theme installed?' ) );
    }
    
    public function testFailIfDirExists()
    {
        touch( self::ROOT_PATH . '/' . self::OUTPUT_DIR );
        $app = new Application("test", "1.0");
        $app->add( new ThemeInstallCommand( null, null, self::ROOT_PATH, __DIR__ . "/../Fixtures/Resources" ) );
        
        $command = $app->find( "silvioq:theme:install" );
        $this->assertInstanceOf( ThemeInstallCommand::class, $command );
        
        $commandTester = new CommandTester($command);
        $commandTester->execute(array(
            'command' => $command->getName(),
            '--output-dir' => self::OUTPUT_DIR,
            'theme' => self::THEME,
         ) );
        
        $output = $commandTester->getDisplay();
        $this->assertEquals( 255, $commandTester->getStatusCode() );
        $this->assertTrue( false !== strpos( $output, 'dir exists. Remove first o re-run this command' ) );
    }
    
    
    
    public function setUp()
    {
        $this->rmdir( self::ROOT_PATH . '/' . self::OUTPUT_DIR);
    }
    
    public function tearDown()
    {
        $this->rmdir( self::ROOT_PATH . '/' . self::OUTPUT_DIR);
    }
    
    private function rmdir( $dir )
    {
        if( is_file( $dir ) )
            unlink( $dir );
        else if( is_dir( $dir ) )
            system("rm -rf ".escapeshellarg($dir));
    }
}
