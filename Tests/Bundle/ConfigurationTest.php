<?php

namespace Silvioq\Bundle\ThemeBundle\Tests;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionConfigurationTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\Configuration;

class ConfigurationTest extends AbstractExtensionConfigurationTestCase
{
    protected function getContainerExtension()
    {
        return new SilvioqThemeExtension();
    }
    
    protected function getConfiguration()
    {
        return new Configuration();
    }
    
    public function testConfigurationOnSilvioqThemeBundle()
    {
        $expectedConf = [
            'notifications' => [
                'enabled' => true,
                'show_icon_on_zero' => true,
                'view_all_path' => 'viewall',
                'badge_type' => null,
             ],
            'help_extension' => true,
            'twig_form_resource' => 'SilvioqThemeBundle:layout:form-theme.html.twig',
            'web_path' => 'themes',
            'theme' => ['adminLTE'],
        ];
        
        $this->assertProcessedConfigurationEquals($expectedConf, [ __DIR__ . "/Fixtures/config.yml" ] );
    }

}

