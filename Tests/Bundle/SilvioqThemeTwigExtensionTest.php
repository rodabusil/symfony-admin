<?php

namespace Silvioq\Bundle\ThemeBundle\Tests;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;
use Silvioq\Component\AdminTool\Twig\BlockExtension;
use Symfony\Component\Asset\Packages;

class SilvioqThemeTwigExtensionTest extends AbstractExtensionTestCase
{
    protected function getContainerExtensions()
    {
        return array(
            new SilvioqThemeExtension()
        );
    }

    /**
     * @dataProvider getTwigExtensions
     */
    public function testTwigExtensions($service, $class)
    {
        $this->load();
        $this->assertContainerBuilderHasService($service, $class);
        $this->assertContainerBuilderHasServiceDefinitionWithTag($service,"twig.extension");
    }

    public function getTwigExtensions()
    {
        return [
            [ "silvioq.theme.path_exists.extension", \Silvioq\Component\Theme\Twig\PathExistsExtension::class ],
            [ "silvioq.theme.macro.extension", \Silvioq\Component\Theme\Twig\MacroExtension::class ],
            [ "silvioq.theme.block.extension", \Silvioq\Component\Theme\Twig\BlockExtension::class ],
            [ "silvioq.theme.asset.extension", \Silvioq\Component\Theme\Twig\AssetThemeExtension::class ],
        ];
    }
    
    public function testParameterInjectionOnTwigBlockExtension()
    {
        $this->load();
        $blockExtension = $this->container->get("silvioq.theme.block.extension");
        $this->assertCount(2, $blockExtension->getGlobals());
    }
    
    public function testPathExistsLoadedExtension()
    {
        $mockRouter = $this->getMockBuilder(\Symfony\Component\Routing\RouterInterface::class)
            ->setMethods(["generate","match","setContext","getContext","getRouteCollection"])
            ->disableOriginalConstructor()
            ->getMock()
            ;
        
        $this->registerService("router", $mockRouter );
        $this->load();
        $peExtension = $this->container->get("silvioq.theme.path_exists.extension");
        $this->assertCount(1, $peExtension->getFunctions());
    }

    public function testAssetThemeArgumentLoaded()
    {
        $this->load();
        $this->assertContainerBuilderHasServiceDefinitionWithArgument("silvioq.theme.asset.extension", 1, "themes/adminLTE");
    }

    public function testAssetThemeLoadedExtension()
    {
        /** @var Packages */
        $mock = $this
                ->getMockBuilder(Packages::class)
                ->disableOriginalConstructor()
                ->getMock();

        $this->registerService("assets.packages", $mock );
        $this->load();

        $mock = $this->container->get('assets.packages');
        $mock->expects($this->once())
            ->method('getUrl')
            ->with("themes/adminLTE/x.css")
            ->will($this->returnValue('/web/themes/myTheme/css/x.css'))
            ;

        $assetExtension = $this->container->get("silvioq.theme.asset.extension");
        $this->assertCount(1, $assetExtension->getFunctions());

        $this->assertNotNull( $assetExtension->getUrl("x.css") );
    }

    public function testBlockParameterNotificationDisabledConfiguration()
    {
        $this->load(['notifications' => [ 'enabled' => false, 'view_all_path' => 'nourl' ] ] );
        $globals = $this->container->get("silvioq.theme.block.extension")->getGlobals();
        $parameters = $globals['silvioq_param'];
        $this->assertFalse( $parameters->getNotificationsEnabled() );
        $this->assertNull( $parameters->getViewAllNotificationsUrl() );
    }

    public function testBlockParameterNotificationEnabledConfiguration()
    {
        $this->load(['notifications' => [ 'enabled' => true, 'view_all_path' => 'all_path', 'badge_type' => 'info' ] ] );
        $globals = $this->container->get("silvioq.theme.block.extension")->getGlobals();
        $parameters = $globals['silvioq_param'];
        $this->assertTrue( $parameters->getNotificationsEnabled() );
        $this->assertSame( 'all_path', $parameters->getViewAllNotificationsUrl() );
        $this->assertSame( 'info', $parameters->getbadgeType() );
    }
}

