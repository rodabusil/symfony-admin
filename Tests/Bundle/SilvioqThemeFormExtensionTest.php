<?php

namespace Silvioq\Bundle\ThemeBundle\Tests;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;

class SilvioqThemeFormExtensionTest extends AbstractExtensionTestCase
{
    protected function getContainerExtensions()
    {
        return array(
            new SilvioqThemeExtension()
        );
    }
    
    /**
     * @dataProvider getFormTypes
     */
    public function testFormType($service, $class)
    {
        $this->load();
        $this->assertContainerBuilderHasService($service, $class);
        $this->assertContainerBuilderHasServiceDefinitionWithTag($service,"form.type");
    }
    
    public function getFormTypes()
    {
        return [
            [ "silvioq.theme.form.type.entity_hidden", \Silvioq\Component\AdminTool\Form\EntityHiddenType::class ],
        ];
    }
    
    public function testHelpFormExtension()
    {
        $this->load( [ "help_extension" => false ] );
        $this->assertContainerBuilderNotHasService("silvioq.form.extension.help");
    }

}

