<?php

namespace Silvioq\Bundle\ThemeBundle\DependencyInjection\Compiler\Tests;

use Silvioq\Bundle\ThemeBundle\DependencyInjection\Compiler\AddBlockPass;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use Silvioq\Bundle\ThemeBundle\DependencyInjection\SilvioqThemeExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Silvioq\Component\Theme\Block\BlockBuilder;
use Silvioq\Component\Theme\Block\Provider\MenuProviderInterface;
use Silvioq\Component\Theme\Block\Provider\UserProviderInterface;
use Silvioq\Component\Theme\Block\Provider\NotificationProviderInterface;
use Symfony\Component\DependencyInjection\Reference;

class BlockPassTest extends AbstractCompilerPassTestCase
{
    protected function registerCompilerPass(ContainerBuilder $container)
    {
        $container->setParameter("silvioq_theme.notificationsEnabled", true);
        $container->addCompilerPass(new AddBlockPass());
    }

    public function testTaggedService()
    {
        $collectingService = $this->registerService('silvioq.theme.block.builder', BlockBuilder::class);
        $mockMenu = $this->getMockBuilder(MenuProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();
        $mockUser = $this->getMockBuilder(UserProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getUser","setUser"])
                    ->getMock();
        $mockNot = $this->getMockBuilder(NotificationProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getNotifications","addNotification"])
                    ->getMock();

        $tagService = $this->registerService('tagged.service', $mockMenu)
                ->addTag('silvioq.theme.block', [ 'type' => 'menu.top' ] )
                ->addTag('silvioq.theme.block', [ 'type' => 'menu.side' ] )
                ;

        $tagService2 = $this->registerService('tagged.service.user', $mockUser)
                ->addTag('silvioq.theme.block', [ 'type' => 'user' ] )
                ;
        $tagService3 = $this->registerService('tagged.service.not', $mockNot)
                ->addTag('silvioq.theme.block', [ 'type' => 'notification' ] )
                ;

        $this->compile();
        $this->assertContainerBuilderHasService('silvioq.theme.block.builder');
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'silvioq.theme.block.builder',
            'addTopMenuProvider',
            [ new Reference('tagged.service') ] );
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'silvioq.theme.block.builder',
            'addSideMenuProvider',
            [ new Reference('tagged.service') ] );
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'silvioq.theme.block.builder',
            'addUserProvider',
            [ new Reference('tagged.service.user') ] );
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            'silvioq.theme.block.builder',
            'addNotificationProvider',
            [ new Reference('tagged.service.not') ] );

        $builder = $this->container->get('silvioq.theme.block.builder');
    }
    
    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage Service tagged.service must declare 'type' on silvioq.theme.block tag
     */
    public function testNotDeclaredTag()
    {
        $collectingService = $this->registerService('silvioq.theme.block.builder', BlockBuilder::class);
        $mockMenu = $this->getMockBuilder(MenuProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();

        $tagService = $this->registerService('tagged.service', $mockMenu)
                ->addTag('silvioq.theme.block', [ ] );

        $this->compile();

    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessage Type invalid.tag is invalid, must be menu.top, menu.side, user or notification
     */
    public function testInvalidTagInTaggedService()
    {
        $collectingService = $this->registerService('silvioq.theme.block.builder', BlockBuilder::class);
        $mockMenu = $this->getMockBuilder(MenuProviderInterface::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();

        $tagService = $this->registerService('tagged.service', $mockMenu)
                ->addTag('silvioq.theme.block', [ 'type' => 'invalid.tag' ] );

        $this->compile();

    }

    /**
     * @expectedException \LogicException
     * @expectedExceptionMessageRegex /Service tagged.service must implements/
     * @dataProvider getValidTypes
     */
    public function testInvalidClassInTaggedService($type)
    {
        $collectingService = $this->registerService('silvioq.theme.block.builder', BlockBuilder::class);
        // MenuItemInterface::class;
        $mockMenu = $this->getMockBuilder(\stdClass::class)
                    ->disableOriginalConstructor()
                    ->setMethods(["getItems","addItem"])
                    ->getMock();

        $tagService = $this->registerService('tagged.service', $mockMenu)
                ->addTag('silvioq.theme.block', [ 'type' => $type ] );

        $this->compile();
    }
    
    public function getValidTypes()
    {
        return [
            [ 'menu.top' ],
            [ 'menu.side' ],
            [ 'notification' ],
            [ 'user' ],
        ];
    }

}
// vim:sw=4 ts=4 sts=4 et
